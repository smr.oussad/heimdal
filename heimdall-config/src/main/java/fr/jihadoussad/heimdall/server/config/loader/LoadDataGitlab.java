package fr.jihadoussad.heimdall.server.config.loader;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
@Profile("gitlab")
public class LoadDataGitlab implements CommandLineRunner {
    private final Logger logger = LoggerFactory.getLogger(LoadDataDev.class);

    private final ApplicationRepository applicationRepository;

    private final PrivilegeRepository privilegeRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public LoadDataGitlab(final ApplicationRepository applicationRepository, final PrivilegeRepository privilegeRepository, final UserRepository userRepository, final PasswordEncoder passwordEncoder) {
        this.applicationRepository = applicationRepository;
        this.privilegeRepository = privilegeRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("************** BEGIN LOAD APP (GITLAB) **************");

        logger.info("Register heimdall in the application");
        final Application heimdall = new Application();
        heimdall.setName("heimdall");
        heimdall.setDefaultPrivilege("APP");
        heimdall.setExpirationToken(30000L);
        applicationRepository.save(heimdall);

        logger.info("Register default privilege in the heimdall application");
        final Privilege appPrivilege = new Privilege();
        appPrivilege.setApplication(heimdall);
        appPrivilege.setName("APP");
        privilegeRepository.save(appPrivilege);

        logger.info("Register appTest from heimdall with login: appTest, password: *****");
        final User appTestUser = new User();
        appTestUser.setLogin("appTest");
        appTestUser.setPassword(passwordEncoder.encode("AppT3st!"));
        appTestUser.setSecretKey("hHXNuboq0e2Wq7FB5nqaHtqkqwSn2FoUQVZkYwinJW8=");
        appTestUser.setApplication(heimdall);
        appTestUser.addPrivileges(appPrivilege);
        userRepository.save(appTestUser);

        logger.info("Register appTest in the mock application");
        final Application appTest = new Application();
        appTest.setName("appTest");
        appTest.setDefaultPrivilege("USER");
        appTest.setExpirationToken(1500L);
        applicationRepository.save(appTest);

        logger.info("Register default privilege in the application");
        final Privilege privilegeTest = new Privilege();
        privilegeTest.setApplication(appTest);
        privilegeTest.setName("USER");
        privilegeRepository.save(privilegeTest);

        logger.info("Register user from appTest with login: userTest, password: *****");
        final User userTest = new User();
        userTest.setLogin("userTest");
        userTest.setPassword(passwordEncoder.encode("userT3st!"));
        userTest.setSecretKey(Base64.getEncoder().encodeToString(SecretKeyGenerator.createHashmacSHA256Key().getEncoded()));
        userTest.setApplication(appTest);
        userTest.addPrivileges(privilegeTest);
        userRepository.save(userTest);

        logger.info("Load private authentication keys...");
        KeyValueStore.save("heimdall-api-internal/target/heimdall-key-test.priv", "heimdall", "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCGPDOXy2KdFxmBVUksjfPLNUvWkaMI4qaXGOnUG0JU5Zs5z/dU7Qu7rk3SwfyhBdlXDwOQ0ycN+gRad7tAQznM4lq7nwhs7tUzpjlORiBjTdOVt4HZUW2qjgzGxpaijkkQIWY2+4lIvJ0BASCb4n6BmgcrcFtHMzfzPRGOqqijmmrzhZtwUrbbo87GfGbfg01JRc7sq7DndnFnX3aS2dBb0J4IQg2fWgzEklFS+H+YoMzH6Zkys7gkjWvLfk21lND6Tqizhvr3RcjRF8p7kz8feZp3W8jdUoXmvPL3rq4crntwcge2RaVjGZWn18OvySVZKhKJtdI6ivXayYnFJdhAgMBAAECggEAfYebM0XbJ3FHhpk2ooXkG99ihKJS1/2jS3ZCIbsUup8Q0lv/34tYnMBzrsqTDIowGPl6aTi+vy2imsrQ81bEIII14hvjYpGN/DKsjOEeqEKeTtS6v+AT+EEeoRFRyHL+19/6CJiNnP+vego2X7VIKRvtcV6m+KWFeZzFRs5svxPSrDCgSXwEu8zdTwArrXqh4qPr67WSQ2Yxsle8HfEmMpjPF+p9ho+2WN7fEnFQ9p6U+Ju6ornL5gPKM8l9lwHn0EnQgyUCiAlleQWiNZwq7cffpBg3Mfz8xGaOLgB4HYXfpvn+BtpOXu6AiUDLFDyuO0OaGIZ0dUpTNBVR98G0EQKBgQC9eVeiWR+sP21mzrOvbF5h2oBizTw29tjIUB4Md8pgX/Mad9cqavOzP7sy+ioOfeZtmuD/5QGUa61Og36jxb1A6YC1wcUmLLpAdGkaQ5FCAXzZjxGy5XRTbKDHfdk3tmC7b4+NcE1Cgsd6Z/yjs96SkKZ8ThM5cDhoID/I4iEYtwKBgQCvxpoXzXXjfxfOz2pggekmKLImrUEehbINhZaSvHgJ/a56bBCVtuLtpBXP5Vbk4KiZ9krpaonm4juNcfs1xAJ8nvbSljthDjEuBRsOxMj8PJeDPnuAF2HJ0q+bz0QKrwoFsg6E1//t8floAZvdUwpnZeEL9CwMXefMsWssgcZIpwKBgQCeKdOYhxMkYxH89uERt8GVp6LyVuZD740IsbBTsg9yocW6O32WQA9Mew/WPvOqgzg+GEJn+g92iOfzmVq2dVNFA2ZY0vaypjWXplUyAmMYGUvSBFa+4kFXfl88B/ayRgC7liXb6Hnl4muH55QCRxytdigzrazojp/Xc8wq8XC1QwKBgAZgxHKcp+xrTifY6tk5nYYe7+frXXOIEl/fOwl0AT8Lj2stxKRKEI3i6E+02KRRaoy7nRg/H2XbS6HkQuM+i4COHBKkFN9+rlYnJUYx6cENScofWEfbkVbLaEfVC3j/xPNT9KbpV4YAw7X8aPVs7/G0hqfyYi7IsYKB5Yul4dX9AoGAZeKq9SP5+SEGmzZ9e5jF6GSYPsld0BdtCY2hugzsN+X00eCCp4gRPWcWq71ChU3uxvLTTfnJvldVKqICoDVtJFjVksarvf34BCqeyVNXW+fP/0xivp16kesgiQs8hHtRcJX33KUEEFRAj5GQ3DbCngexVMaV4RqAMnA0MS9uLCs=");
        KeyValueStore.save("heimdall-api-internal/target/heimdall-key-test.priv", "appTest", "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCYdGrTf5VUiWmvTqLlIRtumreOaOJ73z82s08n6lmk4Ab0gWhHfnCLd4BUhtmEfBOK5tNeX1yTeseCwHONnl6be5gm1pLIm/8raTgeFUJvIRqh1UDGN2K2WgWs5ir1YIZuJE9tCyrOobZhcceISowz6wISFGiqzfLKT30IfU3/2GuKpPMS+LTrfFIY1B5oHoX3LHWN0WrllnBeql7qvLs+zLCS8CMAKmEX47cVM6E9pOhB5qiBFd+02CDrnYfykfTJPCxQ1dqBY99AU3EPE+NatMIDyyI502IO7ph5bbAEGLHOg3WwsdumzamkgQZuSvJYW67DpoB3z2rKzSmEM2vxAgMBAAECggEAPClEtq2UOkX3pKx9b7n+30jCwrn1Fjjk8ysTQ4chVQTq+bXPdtcYhpDj3XdRgwyLkbbBFm6/U5uR+7ECaJJkdyrwwfLO/cdBjNn6wTRWgRApOE+1PgNBOwCPWMmDjk1G/1Po2/kaLWx3UkDaIph4d0x4BrZzD2QyOU/g4WbBTwJ1UCTTKsae9Q7Z4qC1mjUMNwZUzqZ/dCwaIeKyZ4V1l/bfBSKmpdmv7eUz7Rn51/yUPB5g3HfDlSoZr1fsVxnfsbDr+PoEmYN4DN4qAL0CSCZXfzRIS+vXGk2gvCu2jocF7ytIXP3s44tx+5eCXghWt1mJUS85Gj0qPsoBxOh/WQKBgQDgS3/RpmWRIU3Ya2RCPlCTYTBxULHBGlc2m4oEV4iqWtaq7k7Uel5PrUP70ZdzOl46l3KMFi9ZcMDzJnAEh9SLXbssREfSc6aMvKiqOr77bZBD0Nxqi11n+wFvHchNBZ0klM6xs4SiUVHds1bEB5QKbYC14xu+oKzkG/GfWSZabwKBgQCuAUHN0t61hYGdb81DBX1ZJ8lqhasOPT6mA7J+yEMAgQJThqPsELZLB/FMal/T9HGTvoCS22IPRkV5+dKqkOg5xZ2SNtKDyr6X+uKEer0MxxDlpRENFdx6Q7XHKjoySkVrySVbolz5971ZuH187UUzvpX90b9+nSA5jYk8iJdPnwKBgHlmY/iFp9O1wvywo8N3FwWuRRoQIyXxq+LSrRFMj5wlLqu18NTwprtZfMJ/3wlvPjFYZ5eLKnWuocD5vQe2vUPxp61+B6HRFwR12JTK1zQfSUZrdeH1LMlrAouyAwgtUbDLGlT0ZYW1ninxN7VCpM5AFrpcAlhbfzi4Jz+ocSnhAoGBAKXequTSU9yDxLfYYLm33PKFG9qASzxKo5LLyXZT3pL3Z8lJeq9Iiw1hJ+MpL+ozhaVKHvRfTs2ytf39aJWRht2zhG5h9jENrrvfu9h/zAEHLhNLU8K+iSq/quONXGaeKCkEWKlOUAU05PpHBNB2udYGoqKPNFU04Qgb5KugpWSPAoGADg2E9RdTKV+4KyMymx/SjjJOxiU+qV5sSi5CJgMIhk9yMtd8wBLR26H9S10dvbQo3SueQ8KMwvYBXtt+S9hRkd8RkkJyaeCk6ChSlss9PMrTAZxJYBanpNZOODvg2+GI2mWSYW/TPjaGiuVEtIrKheTGnUAxBW9GfrPzZjBbuvY=");

        logger.info("Process flushing ...");
        applicationRepository.flush();
        privilegeRepository.flush();
        userRepository.flush();

        logger.info("************** END LOAD APP **************");
    }
}
