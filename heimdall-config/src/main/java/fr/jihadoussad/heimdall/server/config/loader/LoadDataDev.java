package fr.jihadoussad.heimdall.server.config.loader;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
@Profile("dev")
public class LoadDataDev implements CommandLineRunner {

    private final Logger logger = LoggerFactory.getLogger(LoadDataDev.class);

    private final ApplicationRepository applicationRepository;

    private final PrivilegeRepository privilegeRepository;

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    public LoadDataDev(final ApplicationRepository applicationRepository, final PrivilegeRepository privilegeRepository, final UserRepository userRepository, final PasswordEncoder passwordEncoder) {
        this.applicationRepository = applicationRepository;
        this.privilegeRepository = privilegeRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {
        logger.info("************** BEGIN LOAD APP (DEV) **************");

        logger.info("Register appMock in the mock application");
        final Application appMock = new Application();
        appMock.setName("appMock");
        appMock.setDefaultPrivilege("USER");
        appMock.setExpirationToken(360000L);
        applicationRepository.save(appMock);

        logger.info("Register default privilege in the application");
        final Privilege privilegeMock = new Privilege();
        privilegeMock.setApplication(appMock);
        privilegeMock.setName("USER");
        privilegeRepository.save(privilegeMock);

        logger.info("Register user from appMock with login: userMock, password: *****");
        final User userMock = new User();
        userMock.setLogin("userMock");
        userMock.setPassword(passwordEncoder.encode("userM0ck!"));
        userMock.setSecretKey(Base64.getEncoder().encodeToString(SecretKeyGenerator.createHashmacSHA256Key().getEncoded()));
        userMock.setApplication(appMock);
        userMock.addPrivileges(privilegeMock);
        userRepository.save(userMock);

        logger.info("Register heimdall in the application");
        final Application heimdall = new Application();
        heimdall.setName("heimdall");
        heimdall.setDefaultPrivilege("APP");
        heimdall.setExpirationToken(31_536_000_000L);
        applicationRepository.save(heimdall);

        logger.info("Register default privilege in the heimdall application");
        final Privilege appPrivilege = new Privilege();
        appPrivilege.setApplication(heimdall);
        appPrivilege.setName("APP");
        privilegeRepository.save(appPrivilege);

        logger.info("Register admin privilege in the heimdall application");
        final Privilege adminPrivilege = new Privilege();
        adminPrivilege.setApplication(heimdall);
        adminPrivilege.setName("ADMIN");
        privilegeRepository.save(adminPrivilege);

        logger.info("Register admin from heimdall with login: admin, password: *****");
        final User adminUser = new User();
        adminUser.setLogin("admin");
        adminUser.setPassword(passwordEncoder.encode("Adm1nistr@tor!"));
        adminUser.setSecretKey(Base64.getEncoder().encodeToString(SecretKeyGenerator.createHashmacSHA256Key().getEncoded()));
        adminUser.setApplication(heimdall);
        adminUser.addPrivileges(appPrivilege, adminPrivilege);
        userRepository.save(adminUser);

        logger.info("Register appMock from heimdall with login: appMock, password: *****");
        final User appUser = new User();
        appUser.setLogin("appMock");
        appUser.setPassword(passwordEncoder.encode("AppM0ck!"));
        appUser.setSecretKey(Base64.getEncoder().encodeToString(SecretKeyGenerator.createHashmacSHA256Key().getEncoded()));
        appUser.setApplication(heimdall);
        appUser.addPrivileges(appPrivilege);
        userRepository.save(appUser);

        logger.info("Load private authentication keys...");
        KeyValueStore.save("heimdall-api/target/heimdall-key-dev.priv", "heimdall", "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCCGPDOXy2KdFxmBVUksjfPLNUvWkaMI4qaXGOnUG0JU5Zs5z/dU7Qu7rk3SwfyhBdlXDwOQ0ycN+gRad7tAQznM4lq7nwhs7tUzpjlORiBjTdOVt4HZUW2qjgzGxpaijkkQIWY2+4lIvJ0BASCb4n6BmgcrcFtHMzfzPRGOqqijmmrzhZtwUrbbo87GfGbfg01JRc7sq7DndnFnX3aS2dBb0J4IQg2fWgzEklFS+H+YoMzH6Zkys7gkjWvLfk21lND6Tqizhvr3RcjRF8p7kz8feZp3W8jdUoXmvPL3rq4crntwcge2RaVjGZWn18OvySVZKhKJtdI6ivXayYnFJdhAgMBAAECggEAfYebM0XbJ3FHhpk2ooXkG99ihKJS1/2jS3ZCIbsUup8Q0lv/34tYnMBzrsqTDIowGPl6aTi+vy2imsrQ81bEIII14hvjYpGN/DKsjOEeqEKeTtS6v+AT+EEeoRFRyHL+19/6CJiNnP+vego2X7VIKRvtcV6m+KWFeZzFRs5svxPSrDCgSXwEu8zdTwArrXqh4qPr67WSQ2Yxsle8HfEmMpjPF+p9ho+2WN7fEnFQ9p6U+Ju6ornL5gPKM8l9lwHn0EnQgyUCiAlleQWiNZwq7cffpBg3Mfz8xGaOLgB4HYXfpvn+BtpOXu6AiUDLFDyuO0OaGIZ0dUpTNBVR98G0EQKBgQC9eVeiWR+sP21mzrOvbF5h2oBizTw29tjIUB4Md8pgX/Mad9cqavOzP7sy+ioOfeZtmuD/5QGUa61Og36jxb1A6YC1wcUmLLpAdGkaQ5FCAXzZjxGy5XRTbKDHfdk3tmC7b4+NcE1Cgsd6Z/yjs96SkKZ8ThM5cDhoID/I4iEYtwKBgQCvxpoXzXXjfxfOz2pggekmKLImrUEehbINhZaSvHgJ/a56bBCVtuLtpBXP5Vbk4KiZ9krpaonm4juNcfs1xAJ8nvbSljthDjEuBRsOxMj8PJeDPnuAF2HJ0q+bz0QKrwoFsg6E1//t8floAZvdUwpnZeEL9CwMXefMsWssgcZIpwKBgQCeKdOYhxMkYxH89uERt8GVp6LyVuZD740IsbBTsg9yocW6O32WQA9Mew/WPvOqgzg+GEJn+g92iOfzmVq2dVNFA2ZY0vaypjWXplUyAmMYGUvSBFa+4kFXfl88B/ayRgC7liXb6Hnl4muH55QCRxytdigzrazojp/Xc8wq8XC1QwKBgAZgxHKcp+xrTifY6tk5nYYe7+frXXOIEl/fOwl0AT8Lj2stxKRKEI3i6E+02KRRaoy7nRg/H2XbS6HkQuM+i4COHBKkFN9+rlYnJUYx6cENScofWEfbkVbLaEfVC3j/xPNT9KbpV4YAw7X8aPVs7/G0hqfyYi7IsYKB5Yul4dX9AoGAZeKq9SP5+SEGmzZ9e5jF6GSYPsld0BdtCY2hugzsN+X00eCCp4gRPWcWq71ChU3uxvLTTfnJvldVKqICoDVtJFjVksarvf34BCqeyVNXW+fP/0xivp16kesgiQs8hHtRcJX33KUEEFRAj5GQ3DbCngexVMaV4RqAMnA0MS9uLCs=");
        KeyValueStore.save("heimdall-api/target/heimdall-key-dev.priv", "appMock", "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCb9Dv+5OUW1GjP8ONK7XGSWU6W9SdmqZfgcV0ttH044cL22zxcyWq+mFE++v6GX3KMcwtfW36Ssq+YFjMXGZW5T0oTAnjKirdCjkA/XXvMSf6bxMpIGiKnISygYVpBAon8kiZ1GbBSg4wQmcluvHZ8TJG6DE+8mQWaGadeAn3pggWX9VGoFlG1YQ9qhyDo7cBW/OLyf7cRLqxOIyr7PK6Sb+gSwX+Qd29+P3z6W5iE8YwECsqKFdwicfUoecHPnoep2xV4PVsN4ARQmzeH6czG9t88zqoyOjEyEsS7OxaTbVZhlKeYaGjqD3ae+wSDliga+s+DRTjfHZhFub/4omYZAgMBAAECggEBAI5dyAMpi2Rq5X+mJKf62GnEMzUleSGEAkx20Jvvnzy1A5j8jD8b8+EoBUscMAwUx+ok/6W+TW63xgptgZg91+uB0VpHMGOdfCw61bXCrkfQ6KfkXtCxYx707ts94/liyvqRpJ/Top3AzrU83qDDVNqlv4Z6xLOjcXS6pInydmUZylOVq8+hc0e2f2hUhOqA5IC2c7J594myCTFwBZ8OfVAGD1tcLn2M4MbB+7HGjiDTQpyB0maNKbv4zz5i1EU4rHWQA2FdK1dilfDk0c5gt8vfac7617lM+jO6lovt3047PWW1j20/BhKx2zdtsTexM/+CQmnEqwqpdgznB8x4lwECgYEA1TUbI52mm6pzVVur8kKKDu8lSEoSsyvGbO48fumGMKKWnDwYmD4BJpa3k7S6k/kaoZ7jxk7tNjIh8Pi431BPg4UmNYngK0bK8micvw+q4M5R8ENvkS97IvdBNttfz3YHixlfBa3VSSDnShBqeLGLk4sDxcDnICrNJMP4Od9C3bkCgYEAu0Feew6ptG/3Jg1ib6xx7eDSm9kIUT2WI+5rwwNL4hxI4NK2VO4SHKTaKiJY9smhhkwgfud+R0YTBtsVoKxUC54nzxzuGKxGT7OZQIRAHE8D3DwS94f7orVMI0/q6HIZIyW5pdLo7f8ZLkjN7hT2kZkmokIxpIpyQsan+Iwo+2ECgYBlHFJKibFBkYwwptDciwG+I1ui9L35ioEHl9PIvCaPWgvi+/0N9hnFmyRkQAlbs38Vtqx+l1+XnB7MkKfb/9I1pSeBrGlLHL+SRlpPobZ/9liVgOWgDMxwwKGEynSdhnP9W4LcxpunmpsVXMOhiVGQHxUi62CwuJuhG+7UQzUKwQKBgQC2QVKfPU9Jsux/melkPhiJahgy3hOtVt8KS+WQ4mV6RwNb51rV4P256W36MoIaeXeAShoayl/rKTYdpyEuyGvo/t9KQ6MuWob4zhpsZAwuQW3FbqcbS7xMyl63w/IkmeEDh5Vg454Y/wBYPz5H6xlt7v1eUkUVHDdH7SjRzkGZIQKBgQC0XG9RVNUPwbdV8W0B78FBx0GDeOls9X/27hnHO2IpHgVhkm1e78cDblzvU7NnrBw35WkufB70/nvLg53m0mU2imME5i/VTl2SEYGs4JQ7nQ0oI6kjkRij43HY9y+aMqPUTAEXpuMYJQ46r47GsRx/eFA2f7fE8YKgulSJ1aB7OQ==");

        logger.info("Process flushing...");
        applicationRepository.flush();
        privilegeRepository.flush();
        userRepository.flush();

        logger.info("************** END LOAD APP **************");
    }
}
