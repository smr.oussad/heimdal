package fr.jihadoussad.heimdall.security.server.common;

import fr.jihadoussad.heimdall.security.AbstractJWTAuthorizationFilter;
import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.security.Key;
import java.util.Map;
import java.util.Optional;

public class LocalJWTAuthorizationFilter extends AbstractJWTAuthorizationFilter {

    private final AuthenticationUserService authenticationUserService;

    private final AuthorizationService authorizationService;

    public LocalJWTAuthorizationFilter(final AuthenticationUserService authenticationUserService, final AuthorizationService authorizationService) {
        this.authenticationUserService = authenticationUserService;
        this.authorizationService = authorizationService;
    }

    @Override
    public ResponseEntity<Map<String, Object>> checkToken(final String token) {
        try {
            final Map<String, Object> payload = authorizationService.getPayload(token);
            final String login = Optional.ofNullable(payload.get("sub")).orElseThrow(() -> new InvalidTokenException(new NullPointerException())).toString();
            final String appName = Optional.ofNullable(payload.get("appName")).orElseThrow(() -> new InvalidTokenException(new NullPointerException())).toString();
            final Key tokenKey = SecretKeyGenerator.toKey(authenticationUserService.getKey(login, appName));

            return ResponseEntity.ok(authorizationService.verifyToken(token, tokenKey));
        } catch (final ContractValidationException e) {
            return ResponseEntity.notFound().build();
        } catch (final InvalidTokenException | ExpiredJwtException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
}
