package fr.jihadoussad.heimdall.security.server.web;

import fr.jihadoussad.heimdall.security.AbstractWebSecurity;
import fr.jihadoussad.heimdall.security.AbstractJWTAuthorizationFilter;
import fr.jihadoussad.heimdall.security.server.common.LocalJWTAuthorizationFilter;
import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@Configuration
@EnableWebSecurity
public class ServerWebSecurity extends AbstractWebSecurity {

    private final AuthenticationUserService authenticationUserService;

    private final AuthorizationService authorizationService;

    public ServerWebSecurity(final AuthenticationUserService authenticationUserService,
                             final AuthorizationService authorizationService) {
        this.authenticationUserService = authenticationUserService;
        this.authorizationService = authorizationService;
    }

    @Override
    public AbstractJWTAuthorizationFilter jwtAuthorizationFilter() {
        return new LocalJWTAuthorizationFilter(authenticationUserService, authorizationService);
    }
}
