package fr.jihadoussad.heimdall.security.server.http;

import fr.jihadoussad.heimdall.security.AbstractHttpSecurity;
import fr.jihadoussad.heimdall.security.AbstractJWTAuthorizationFilter;
import fr.jihadoussad.heimdall.security.server.common.LocalJWTAuthorizationFilter;
import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class ServerHttpSecurity extends AbstractHttpSecurity {

    private final AuthenticationUserService authenticationUserService;

    private final AuthorizationService authorizationService;

    public ServerHttpSecurity(final AuthenticationUserService authenticationUserService,
                          final AuthorizationService authorizationService) {
        this.authenticationUserService = authenticationUserService;
        this.authorizationService = authorizationService;
    }

    @Override
    public AbstractJWTAuthorizationFilter jwtAuthorizationFilter() {
        return new LocalJWTAuthorizationFilter(authenticationUserService, authorizationService);
    }
}
