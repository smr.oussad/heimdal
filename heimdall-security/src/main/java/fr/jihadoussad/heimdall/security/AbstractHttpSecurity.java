package fr.jihadoussad.heimdall.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public abstract class AbstractHttpSecurity extends WebSecurityConfigurerAdapter {

    @Value("${heimdall.signin.url}")
    public String signInUrl;

    @Value("${heimdall.signup.url}")
    public String signUpUrl;

    @Value("${heimdall.public.url:#{null}}")
    public String publicUrl;

    protected abstract AbstractJWTAuthorizationFilter jwtAuthorizationFilter();

    @Override
    @Profile("dev")
    public void configure(final WebSecurity web) {
        web.ignoring().antMatchers("/h2-console/**");
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        if (publicUrl != null) {
            for (String url : publicUrl.split(",")) {
                http.authorizeRequests().antMatchers(HttpMethod.GET, url).permitAll();
            }
        }

        // Disable web security against CORS and CSRF attack
        http.csrf().disable();
        http.cors().disable();

        // Setting authorization configuration
        http.addFilterAfter(jwtAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, signUpUrl).permitAll()
                .antMatchers(HttpMethod.GET, signInUrl).permitAll()
                .anyRequest().authenticated();
    }
}
