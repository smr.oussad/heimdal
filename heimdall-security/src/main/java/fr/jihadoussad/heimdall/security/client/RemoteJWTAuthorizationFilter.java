package fr.jihadoussad.heimdall.security.client;

import fr.jihadoussad.heimdall.client.HeimdallClient;
import fr.jihadoussad.heimdall.security.AbstractJWTAuthorizationFilter;
import org.springframework.http.ResponseEntity;

import java.util.Map;

public class RemoteJWTAuthorizationFilter extends AbstractJWTAuthorizationFilter {

    private final HeimdallClient heimdallClient;

    public RemoteJWTAuthorizationFilter(final HeimdallClient heimdallClient) {
        this.heimdallClient = heimdallClient;
    }

    @Override
    public ResponseEntity<Map<String, Object>> checkToken(final String token) {
        return heimdallClient.checkToken(token);
    }
}
