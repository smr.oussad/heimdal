package fr.jihadoussad.heimdall.security.client;

import fr.jihadoussad.heimdall.client.HeimdallClient;
import fr.jihadoussad.heimdall.security.AbstractWebSecurity;
import fr.jihadoussad.heimdall.security.AbstractJWTAuthorizationFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@Configuration
@EnableWebSecurity
public class ClientWebSecurity extends AbstractWebSecurity {

    private final HeimdallClient heimdallClient;

    public ClientWebSecurity(final HeimdallClient heimdallClient) {
        this.heimdallClient = heimdallClient;
    }

    @Override
    public AbstractJWTAuthorizationFilter jwtAuthorizationFilter() {
        return new RemoteJWTAuthorizationFilter(heimdallClient);
    }
}