package fr.jihadoussad.heimdall.server.internal.api.controller;

import fr.jihadoussad.heimdall.server.contract.AuthenticationApplicationService;
import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Key;
import java.util.*;

@RestController
@RequestMapping("oauth2")
public class OAuth2Controller {

    private final Logger logger = LoggerFactory.getLogger(OAuth2Controller.class);

    private final AuthenticationApplicationService authenticationApplicationService;

    private final AuthenticationUserService authenticationUserService;

    private final AuthorizationService authorizationService;

    public OAuth2Controller(final AuthenticationApplicationService authenticationApplicationService,
                            final AuthenticationUserService authenticationUserService,
                            final AuthorizationService authorizationService) {
        this.authenticationApplicationService = authenticationApplicationService;
        this.authenticationUserService = authenticationUserService;
        this.authorizationService = authorizationService;
    }

    @ApiOperation(value="Authenticate and generate token jwt", response = String.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message="Successful authenticate"),
            @ApiResponse(code=400, message="Contract input violation"),
            @ApiResponse(code=403, message ="Access denied"),
            @ApiResponse(code=500, message="Technical error")
    })
    @GetMapping(value = "/{appName}/accessToken", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> authenticate(@PathVariable("appName") final String appName,
                                               @RequestParam("login") final String login, @RequestParam("password") final String password)
            throws ContractValidationException, IOException {
        logger.info("BEGIN - Process {} authentication with following credential input: login = {}, password = *******", appName, login);
        logger.debug("{} authentication input: login = {}, password = {}", appName, login, password);

        final List<String> privileges = Optional.ofNullable(authenticationUserService.authenticate(login, appName, password))
                .orElse(Collections.emptyList());

        logger.info("Calling the authentication service to get the secret key");
        final Key tokenKey = SecretKeyGenerator.toKey(authenticationUserService.getKey(login, appName));

        logger.info("Calling authentication application service to get expiration authentication token user");
        final Long expiration = authenticationApplicationService.getExpirationToken(appName);

        logger.info("Calling authorization service to generate the token : {}[tokenKey = {}, expiration = {}, login = {}, privileges = {}]", appName, tokenKey, expiration, login, privileges);
        final ResponseEntity<String> response = ResponseEntity.ok(authorizationService.generateToken(tokenKey, expiration, login, appName, privileges.toArray(String[]::new)));

        logger.debug("{} authentication response: token = {}", appName, Objects.requireNonNull(response.getBody()));
        logger.info("END - Successfully {} authentication process. Token = *******", appName);
        return response;
    }

    @ApiOperation(value="Check token jwt validity", response = String.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message="Token valid"),
            @ApiResponse(code=400, message="Contract input violation"),
            @ApiResponse(code=401, message="Invalid or expired token"),
            @ApiResponse(code=403, message ="Access denied"),
            @ApiResponse(code=500, message="Technical error")
    })
    @GetMapping(value = "/{appName}/checkToken", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission(#appName, 'ROLE_APP_HEIMDALL')")
    public ResponseEntity<Map<String, Object>> checkToken(@PathVariable("appName") @P("appName") final String appName, @Param("token") final String token)
            throws InvalidTokenException, ContractValidationException {
        logger.info("BEGIN - Process {} check token with following token input: token = *******", appName);
        logger.debug("{} token checker input: token = {}", appName, token);

        logger.info("Calling authorization service to extract token payload");
        final Map<String, Object> tokenPayload = authorizationService.getPayload(token);
        final String login = Optional.ofNullable(tokenPayload.get("sub")).orElseThrow(() -> new InvalidTokenException(new NullPointerException())).toString();

        logger.info("Calling the authentication service to get the secret key");
        final Key tokenKey = SecretKeyGenerator.toKey(authenticationUserService.getKey(login, appName));

        logger.info("Calling authorization service to check the following validity token: {}", token);
        final ResponseEntity<Map<String, Object>> response = ResponseEntity.ok(authorizationService.verifyToken(token, tokenKey));

        logger.debug("{} token checker response: token = {}", appName, Objects.requireNonNull(response.getBody()));
        logger.info("END - Successfully {} check token process. Token = *******", appName);
        return response;
    }
}
