package fr.jihadoussad.heimdall.server.internal.api.controller;

import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;

@RestControllerAdvice
public class InterceptorController {

    private final Logger logger = LoggerFactory.getLogger(InterceptorController.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<HeimdallResponse> internalError(final Exception e) {
        logger.error("Technical error was occurred: ", e);
        return new ResponseEntity<>(new HeimdallResponse(TECHNICAL_ERROR.code, TECHNICAL_ERROR.message), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<Void> httpRequestError(final HttpRequestMethodNotSupportedException e) {
        logger.debug(e.getMessage());
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<String> httpMediaTypeError(final HttpMediaTypeNotSupportedException e) {
        logger.debug(e.getMessage());
        return ResponseEntity.notFound().build();
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<HeimdallResponse> missingRequestFieldsError(final MissingServletRequestParameterException e) {
        logger.debug(e.getMessage());
        return new ResponseEntity<>(new HeimdallResponse(REQUIRED_FIELD.code, REQUIRED_FIELD.message + e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<HeimdallResponse> accessDeniedError(final AccessDeniedException e) {
        logger.debug(e.getMessage());
        return new ResponseEntity<>(new HeimdallResponse(APP_NOT_FOUND.code, APP_NOT_FOUND.message), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ContractValidationException.class)
    public ResponseEntity<HeimdallResponse> contractViolation(final ContractValidationException e) {
        logger.info("END - " + e);
        final HttpStatus httpStatus = (e.getCode().equals(APP_NOT_FOUND.code) ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(new HeimdallResponse(e.getCode(), e.getMessage()), httpStatus);
    }

    @ExceptionHandler(InvalidTokenException.class)
    public ResponseEntity<HeimdallResponse> invalidToken(final InvalidTokenException e) {
        logger.info(e.getMessage());
        return new ResponseEntity<>(new HeimdallResponse(INVALID_TOKEN.code, INVALID_TOKEN.message), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(ExpiredJwtException.class)
    public ResponseEntity<HeimdallResponse> jwtExpired(final ExpiredJwtException e) {
        logger.info(e.getMessage());
        return new ResponseEntity<>(new HeimdallResponse(TOKEN_EXPIRED.code, TOKEN_EXPIRED.message), HttpStatus.UNAUTHORIZED);
    }
}
