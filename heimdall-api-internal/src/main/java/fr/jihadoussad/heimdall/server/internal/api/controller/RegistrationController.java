package fr.jihadoussad.heimdall.server.internal.api.controller;

import fr.jihadoussad.heimdall.server.contract.AuthenticationApplicationService;
import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@RestController
@RequestMapping("register")
public class RegistrationController {

    private final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private final AuthorizationService authorizationService;

    private final AuthenticationApplicationService authenticationApplicationService;

    private final AuthenticationUserService authenticationUserService;

    public RegistrationController(final AuthenticationApplicationService authenticationApplicationService,
                                  final AuthorizationService authorizationService, final AuthenticationUserService authenticationUserService) {
        this.authenticationApplicationService = authenticationApplicationService;
        this.authenticationUserService = authenticationUserService;
        this.authorizationService = authorizationService;
    }

    @ApiOperation(value="Register application", response = String.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message="Successful user register"),
            @ApiResponse(code=400, message="Contract input violation"),
            @ApiResponse(code=403, message="Access denied"),
            @ApiResponse(code=500, message="Technical error")
    })
    @PostMapping(value = "/app/{appName}/user", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasPermission(#appName, 'ROLE_APP_HEIMDALL')")
    public ResponseEntity<String> registerUser(@PathVariable("appName") @P("appName") final String appName,
                                               @Param("login") final String login, @RequestParam("password")  final String encodedPassword,
                                               @RequestParam(value = "privileges", required = false) final List<String> privileges)
                    throws ContractValidationException, IOException, NoSuchAlgorithmException {
        final List<String> collectPrivileges = Optional.ofNullable(privileges).orElse(Collections.emptyList());
        logger.info("BEGIN - Process {} registration user with following input: login = {}, password = *******, privileges = [{}]",
                appName, login, collectPrivileges);
        logger.debug("{} registration user input: login = {}, password = {}, privileges = [{}]",
                appName, login, encodedPassword, collectPrivileges);

        logger.info("Process to token secret key generation...");
        final Key secretKey = SecretKeyGenerator.createHashmacSHA256Key();
        logger.debug("Secret key token generated: {}", Base64.getEncoder().encodeToString(secretKey.getEncoded()));

        logger.info("Call authentication user service to register user: {}[login = {}, password = *******, privileges = {}]", appName, login, collectPrivileges);
        final List<String> actualPrivileges = authenticationUserService.registerUser(login, encodedPassword, Base64.getEncoder().encodeToString(secretKey.getEncoded()), appName,
                collectPrivileges.toArray(String[]::new));

        logger.info("Calling authentication application service to get expiration authentication token user");
        final Long expiration = authenticationApplicationService.getExpirationToken(appName);

        logger.info("Generating token jwt.");
        final ResponseEntity<String> response = ResponseEntity.ok(authorizationService.generateToken(secretKey, expiration, login, appName, actualPrivileges.toArray(String[]::new)));

        logger.debug("Registration user response: Token = {}", Objects.requireNonNull(response.getBody()));
        logger.info("END - Successfully {} registration user process. Token = *******", appName);
        return response;
    }
}
