package fr.jihadoussad.heimdall.server.internal.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.heimdall.server.internal.api.HeimdallInternalApplication;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.concurrent.atomic.AtomicReference;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = HeimdallInternalApplication.class)
@ComponentScan(basePackages = {"fr.jihadoussad.heimdall.server.*"})
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class CommonIntegrationTest {

    protected static final String PUBLIC_KEY_HEIMDALL = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAghjwzl8tinRcZgVVJLI3zyzVL1pGjCOKmlxjp1BtCVOWbOc/3VO0Lu65N0sH8oQXZVw8DkNMnDfoEWne7QEM5zOJau58IbO7VM6Y5TkYgY03TlbeB2VFtqo4MxsaWoo5JECFmNvuJSLydAQEgm+J+gZoHK3BbRzM38z0Rjqqoo5pq84WbcFK226POxnxm34NNSUXO7Kuw53ZxZ192ktnQW9CeCEINn1oMxJJRUvh/mKDMx+mZMrO4JI1ry35NtZTQ+k6os4b690XI0RfKe5M/H3mad1vI3VKF5rzy966uHK57cHIHtkWlYxmVp9fDr8klWSoSibXSOor12smJxSXYQIDAQAB";
    protected static final String PUBLIC_KEY_TEST = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmHRq03+VVIlpr06i5SEbbpq3jmjie98/NrNPJ+pZpOAG9IFoR35wi3eAVIbZhHwTiubTXl9ck3rHgsBzjZ5em3uYJtaSyJv/K2k4HhVCbyEaodVAxjditloFrOYq9WCGbiRPbQsqzqG2YXHHiEqMM+sCEhRoqs3yyk99CH1N/9hriqTzEvi063xSGNQeaB6F9yx1jdFq5ZZwXqpe6ry7PsywkvAjACphF+O3FTOhPaToQeaogRXftNgg652H8pH0yTwsUNXagWPfQFNxDxPjWrTCA8siOdNiDu6YeW2wBBixzoN1sLHbps2ppIEGbkryWFuuw6aAd89qys0phDNr8QIDAQAB";
    protected static final String URL_OAUTH2_SERVER = "/oauth2";
    protected static final String URL_REGISTER_SERVER = "/register";

    protected static final ObjectMapper OBJECT_MAPPER = Jackson2ObjectMapperBuilder.json().build();

    @Autowired
    protected MockMvc mvc;

    protected String appToken;

    @BeforeEach
    public void getToken() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_HEIMDALL, "AppT3st!");
        final AtomicReference<String> token = new AtomicReference<>("");

        mvc.perform(get(URL_OAUTH2_SERVER + "/heimdall/accessToken?login=appTest&password=" + encodedMessage)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        appToken = token.get();
    }
}
