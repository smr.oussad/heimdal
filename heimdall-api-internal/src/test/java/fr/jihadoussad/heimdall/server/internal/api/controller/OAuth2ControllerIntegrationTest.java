package fr.jihadoussad.heimdall.server.internal.api.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.Base64;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OAuth2ControllerIntegrationTest extends CommonIntegrationTest {

    @Test
    public void authenticateWhenUserNotFoundTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=foo&password=superCipherP@ssw0rd")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void authenticateWhitEncipherPasswordTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=userTest&password=superCipherP@ssw0rd")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_PASSWORD_CIPHER.code);
    }

    @Test
    public void authenticateWhenPasswordIncorrectTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "superCipherP@ssw0rd");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=userTest&password=" + encodedMessage)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(PASSWORD_INCORRECT.code);
    }

    @Test
    public void authenticateTest() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "userT3st!");
        final AtomicReference<String> token = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=userTest&password=" + encodedMessage)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        mvc.perform(get( "/oauth2/appTest/checkToken?token=" + token.get())
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void checkTokenInvalidTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get("/oauth2/appTest/checkToken?token=my.hack.token")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isUnauthorized());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void checkTokenAlteredTest() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "userT3st!");
        final AtomicReference<String> token = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=userTest&password=" + encodedMessage)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        // Alter privileges in the token
        final String[] arrayJwt = token.get().split("\\.");

        final String jsonPayload = new String(Base64.getDecoder().decode(arrayJwt[1]));
        final ObjectMapper objectMapper = new ObjectMapper();
        final Map<String, Object> payload = objectMapper.readValue(jsonPayload, new TypeReference<Map<String, Object>>(){});
        payload.put("privileges", "adminTest");

        final String alterJwt = arrayJwt[0] + "." + Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(payload)) + "."
                + arrayJwt[2];

        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get("/oauth2/appTest/checkToken?token=" + alterJwt)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isUnauthorized());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void checkTokenExpiredTest() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "userT3st!");
        final AtomicReference<String> token = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/appTest/accessToken?login=userTest&password=" + encodedMessage)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        Thread.sleep(1500);

        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get("/oauth2/appTest/checkToken?token=" + token.get())
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isUnauthorized());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(TOKEN_EXPIRED.code);
    }
}
