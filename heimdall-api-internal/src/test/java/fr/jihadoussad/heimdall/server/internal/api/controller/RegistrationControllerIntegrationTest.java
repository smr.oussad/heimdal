package fr.jihadoussad.heimdall.server.internal.api.controller;

import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.concurrent.atomic.AtomicReference;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class RegistrationControllerIntegrationTest extends CommonIntegrationTest {

    @Test
    public void registerUserWhenExistTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=userTest&password=superCipherPassword&privileges=USER")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(USER_EXIST.code);
    }

    @Test
    public void registerUserWhenAppNotExistOrNotAllowedTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/unknownApp/user?login=toto&password=superCipherPassword")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isNotFound());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(APP_NOT_FOUND.code);
    }

    @Test
    public void registerUserWithEncipheringPasswordTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=toto&password=superCipherP@ssword")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_PASSWORD_CIPHER.code);
    }

    @Test
    public void registerUserWithUnknownPrivilegeTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "superCipherP@ssw0rd");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=tutu&password=" + encodedMessage + "&privileges=FOO")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(PRIV_NOT_FOUND.code);
    }

    @Test
    public void registerUserWithDefaultPrivilegeFilledTest() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "superCipherP@ssw0rd");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=toto&password=" + encodedMessage + "&privileges=USER")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void registerUserWithoutDefaultPrivilegeFilledTest() throws Exception {
        final String encodedMessage = KeyPairGenerator.cipher(PUBLIC_KEY_TEST, "superCipherP@ssw0rd");
        final AtomicReference<String> token = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=tutu&password=" + encodedMessage)
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> token.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isOk());

        mvc.perform(get("/oauth2/appTest/checkToken?token=" + token.get())
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
