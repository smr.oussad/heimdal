package fr.jihadoussad.heimdall.server.provider.factories;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;

public class UserFactory {

    private static final UserFactory INSTANCE = new UserFactory();

    private static User user;

    private UserFactory() {}

    public static UserFactory getInstance() {
        user = new User();
        return INSTANCE;
    }

    public UserFactory application(final Application application) {
        user.setApplication(application);
        return INSTANCE;
    }

    public UserFactory login(final String login) {
        user.setLogin(login);
        return INSTANCE;
    }

    public UserFactory password(final String password) {
        user.setPassword(password);
        return INSTANCE;
    }

    public UserFactory secretKey(final String secretKey) {
        user.setSecretKey(secretKey);
        return INSTANCE;
    }

    public UserFactory privileges(final Privilege... privileges) {
        user.addPrivileges(privileges);
        return INSTANCE;
    }

    public User build() {
        return user;
    }
}
