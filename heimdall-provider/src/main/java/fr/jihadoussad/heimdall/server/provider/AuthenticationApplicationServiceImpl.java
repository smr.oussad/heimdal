package fr.jihadoussad.heimdall.server.provider;

import fr.jihadoussad.heimdall.server.contract.AuthenticationApplicationService;
import fr.jihadoussad.heimdall.server.contract.RegistrationAppOutput;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.server.provider.factories.ApplicationFactory;
import fr.jihadoussad.heimdall.server.provider.factories.PrivilegeFactory;
import fr.jihadoussad.heimdall.server.provider.factories.UserFactory;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import fr.jihadoussad.heimdall.utils.validators.InputValidator;
import fr.jihadoussad.heimdall.utils.validators.RegexValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;

@Service
public class AuthenticationApplicationServiceImpl implements AuthenticationApplicationService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private PrivilegeRepository privilegeRepository;

    @Resource
    private ApplicationRepository applicationRepository;

    @Value("${heimdall.priv.key.path}")
    private String privateKeyPath;

    private final PasswordEncoder passwordEncoder;

    public AuthenticationApplicationServiceImpl(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void banUser(final String login, final String appName) {
        // Detach user privileges after deleted
        Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .ifPresent(u -> u.removePrivileges(u.getPrivileges().toArray(Privilege[]::new)));

        // Process deleting user
        userRepository.deleteUserByLoginAndApplication_Name(login, appName);
    }

    @Override
    public RegistrationAppOutput registerApplication(final String appName, final String password,
                                                     final String defaultPrivilege, final String secretKey,
                                                     final Long expirationToken)
            throws ContractValidationException, NoSuchAlgorithmException, IOException {
        // Validate appName as heimdall login
        if (!InputValidator.getInstance(RegexValidator.LOGIN).validate(appName)) {
            throw new ContractValidationException(INVALID_LOGIN.message, INVALID_LOGIN.code);
        }

        // Check if app name already exist
        if (applicationRepository.findApplicationByName(appName) != null) {
            throw new ContractValidationException(APP_EXIST.message, APP_EXIST.code);
        }

        // Validate password
        if (!InputValidator.getInstance(RegexValidator.PASSWORD).validate(password)) {
            throw new ContractValidationException(INVALID_PASSWORD.message, INVALID_PASSWORD.code);
        }

        // Build Application entity
        final Application application = ApplicationFactory
                .getInstance()
                .name(appName)
                .defaultPrivilege(defaultPrivilege)
                .expirationToken(expirationToken)
                .build();

        // Process registration
        applicationRepository.save(application);

        // Build default privilege entity
        final Privilege privilege = PrivilegeFactory
                .getInstance()
                .name(defaultPrivilege)
                .application(application)
                .build();

        // Process adding default privilege
        privilegeRepository.save(privilege);

        final Application heimdall = applicationRepository.findApplicationByName("heimdall");

        // Get the default heimdall default privilege
        final Privilege defaultHeimdallPrivilege = privilegeRepository.findPrivilegeByNameAndApplication_Name(heimdall.getDefaultPrivilege(), "heimdall");

        // Build Heimdall user
        final User user = UserFactory
                .getInstance()
                .application(heimdall)
                .login(appName)
                .password(passwordEncoder.encode(password))
                .privileges(defaultHeimdallPrivilege)
                .secretKey(secretKey)
                .build();

        //Process adding heimdall user app
        userRepository.save(user);

        // Flushing
        applicationRepository.flush();
        privilegeRepository.flush();
        userRepository.flush();

        // Process to keyPair generation and extract the private key
        final KeyPair keyPair = KeyPairGenerator.createRSAKeyPair();
        final String privateKey = Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded());

        // Store it with other auth keys
        KeyValueStore.save(privateKeyPath, appName, privateKey);

        // Returning the public key generated + privileges list
        return new RegistrationAppOutput(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()), Collections.singletonList(heimdall.getDefaultPrivilege()));
    }

    @Override
    public void unsubscribeApplication(final String appName){
        // Process deleting application
        applicationRepository.deleteApplicationByName(appName);
    }

    @Override
    public void addPrivileges(final String appName, final String... privileges) throws ContractValidationException {
        // Check if application exist
        final Application application = Optional.ofNullable(applicationRepository.findApplicationByName(appName))
                .orElseThrow(() -> new ContractValidationException(APP_NOT_FOUND.message, APP_NOT_FOUND.code));

        final List<Privilege> collectPrivileges = Arrays.stream(privileges)
                .map(privilege -> privilegeRepository.findPrivilegeByNameAndApplication_Name(privilege, appName))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        // Check if privilege(s) exist for this application
        if (!collectPrivileges.isEmpty()) {
            throw new ContractValidationException(PRIV_EXIST_APP.message + collectPrivileges.stream().map(Privilege::getName).toString(), PRIV_EXIST_APP.code);
        }

        // Build Privileges entities and persist
        Arrays.stream(privileges)
                .forEach(name -> {
                    final Privilege privilege = PrivilegeFactory
                            .getInstance()
                            .name(name)
                            .application(application)
                            .build();
                    privilegeRepository.saveAndFlush(privilege);
                });
    }

    @Override
    public void removePrivileges(final String appName, final String... privileges) throws ContractValidationException {
        final String defaultPrivilege = Optional.ofNullable(applicationRepository.findApplicationByName(appName))
                .map(Application::getDefaultPrivilege)
                .orElseThrow(() -> new ContractValidationException(APP_NOT_FOUND.message, APP_NOT_FOUND.code));

        // Check if privileges not containing default privilege
        if (Arrays.asList(privileges).contains(defaultPrivilege)) {
            throw new ContractValidationException(DEF_PRIV_NOT_DELETED.message, DEF_PRIV_NOT_DELETED.code);
        }

        // Process deleting privileges
        Arrays.stream(privileges)
                .forEach(privilege -> {
                    privilegeRepository.findPrivilegeByNameAndApplication_Name(privilege, appName)
                            .removeUsers(); // Detaching all users
                    privilegeRepository.flush();
                    privilegeRepository.deletePrivilegeByNameAndApplication_Name(privilege, appName);
                });
    }

    @Override
    public Long getExpirationToken(String appName) throws ContractValidationException {
        return Optional.ofNullable(applicationRepository.findApplicationByName(appName))
                       .map(Application::getExpirationToken)
                       .orElseThrow(() -> new ContractValidationException(APP_NOT_FOUND.message, APP_NOT_FOUND.code));
    }
}
