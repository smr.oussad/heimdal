package fr.jihadoussad.heimdall.server.provider.factories;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;

public class PrivilegeFactory {

    private static final PrivilegeFactory INSTANCE = new PrivilegeFactory();

    private static Privilege privilege;

    private PrivilegeFactory() {}

    public static PrivilegeFactory getInstance() {
        privilege = new Privilege();
        return INSTANCE;
    }

    public PrivilegeFactory name(final String name) {
        privilege.setName(name);
        return INSTANCE;
    }

    public PrivilegeFactory application(final Application application) {
        privilege.setApplication(application);
        return INSTANCE;
    }

    public Privilege build() {
        return privilege;
    }
}
