package fr.jihadoussad.heimdall.server.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Base64;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {

    private final ObjectMapper objectMapper;

    public AuthorizationServiceImpl(final ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    @Override
    public String generateToken(final Key secretKey, final long expiration, final String login, final String appName, final String... privileges) {
        final Date now = new Date();
        final Date exp = new Date(System.currentTimeMillis() + expiration);
        final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
        return Jwts.builder()
                .setId(UUID.randomUUID().toString().replace("-", ""))
                .setIssuedAt(now)
                .setNotBefore(now)
                .setExpiration(exp)
                .setSubject(login)
                .claim("appName", appName)
                .claim("privileges", privileges)
                .signWith(secretKey, signatureAlgorithm)
                .compact();
    }

    @Override
    public Map<String, Object> getPayload(final String token) throws InvalidTokenException {
        try {
            final String jsonPayload = new String(Base64.getDecoder().decode(token.split("\\.")[1]));
            return objectMapper.readValue(jsonPayload, new TypeReference<Map<String, Object>>(){});
        } catch (final ArrayIndexOutOfBoundsException | JsonProcessingException e) {
            throw new InvalidTokenException(e);
        }
    }

    @Override
    public Map<String, Object> verifyToken(final String token, final Key secretKey) throws InvalidTokenException, ExpiredJwtException {
        try {
            final Jws<Claims> jws = Jwts.parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token);

            return jws.getBody();
        } catch (final UnsupportedJwtException | MalformedJwtException | SignatureException e) {
            throw new InvalidTokenException(e);
        }
    }
}
