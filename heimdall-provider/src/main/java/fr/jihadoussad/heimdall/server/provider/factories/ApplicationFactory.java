package fr.jihadoussad.heimdall.server.provider.factories;

import fr.jihadoussad.heimdall.server.core.entities.Application;

public class ApplicationFactory {

    private static final ApplicationFactory INSTANCE = new ApplicationFactory();

    private static Application application;

    private ApplicationFactory() {

    }

    public static ApplicationFactory getInstance() {
        application = new Application();
        return INSTANCE;
    }

    public ApplicationFactory name(final String name) {
        application.setName(name);
        return INSTANCE;
    }

    public ApplicationFactory defaultPrivilege(final String defaultPrivilege) {
        application.setDefaultPrivilege(defaultPrivilege);
        return INSTANCE;
    }

    public ApplicationFactory expirationToken(final Long expirationToken) {
        application.setExpirationToken(expirationToken);
        return INSTANCE;
    }

    public Application build() {
        return application;
    }
}
