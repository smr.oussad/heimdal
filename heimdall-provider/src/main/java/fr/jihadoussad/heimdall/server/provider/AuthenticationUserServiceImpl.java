package fr.jihadoussad.heimdall.server.provider;

import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.server.provider.factories.UserFactory;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import fr.jihadoussad.heimdall.utils.validators.InputValidator;
import fr.jihadoussad.heimdall.utils.validators.RegexValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;

@Service
public class AuthenticationUserServiceImpl implements AuthenticationUserService {

    @Resource
    private UserRepository userRepository;

    @Resource
    private PrivilegeRepository privilegeRepository;

    @Resource
    private ApplicationRepository applicationRepository;

    @Value("${heimdall.priv.key.path}")
    private String privateKeyPath;

    private final PasswordEncoder passwordEncoder;

    public AuthenticationUserServiceImpl(final PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<String> registerUser(final String login, final String encipheringPassword, final String secretKey, final String appName, final String... privileges)
            throws ContractValidationException, IOException {
        // Validate login
        if (!InputValidator.getInstance(RegexValidator.LOGIN).validate(login)) {
            throw new ContractValidationException(INVALID_LOGIN.message, INVALID_LOGIN.code);
        }

        // Check if login already exist
        if(userRepository.findUserByLoginAndApplication_Name(login, appName) != null) {
            throw new ContractValidationException(USER_EXIST.message, USER_EXIST.code);
        }

        // Check if application exist
        final Application application = Optional.ofNullable(applicationRepository.findApplicationByName(appName))
                .orElseThrow(() -> new ContractValidationException(APP_NOT_FOUND.message, APP_NOT_FOUND.code));

        try {
            // Deciphering the password
            final String password = KeyPairGenerator.decipher(KeyValueStore.load(privateKeyPath, appName), encipheringPassword);

            // Validate password
            if (!InputValidator.getInstance(RegexValidator.PASSWORD).validate(password)) {
                throw new ContractValidationException(INVALID_PASSWORD.message, INVALID_PASSWORD.code);
            }

            final List<Privilege> collectPrivileges = Arrays.stream(privileges)
                    .map(privilege -> privilegeRepository.findPrivilegeByNameAndApplication_Name(privilege, appName))
                    .collect(Collectors.toList());
            // Check if privilege(s) exist for this application
            if (collectPrivileges.contains(null)) {
                throw new ContractValidationException(PRIV_NOT_FOUND.message, PRIV_NOT_FOUND.code);
            }

            // Add default privilege if not exist
            if(collectPrivileges.stream().noneMatch(privilege -> privilege.getName().equals(application.getDefaultPrivilege()))) {
                collectPrivileges.add(privilegeRepository.findPrivilegeByNameAndApplication_Name(application.getDefaultPrivilege(), appName));
            }

            // Build User entity
            final User user = UserFactory
                    .getInstance()
                    .login(login)
                    .password(passwordEncoder.encode(password))
                    .secretKey(secretKey)
                    .privileges(collectPrivileges.toArray(Privilege[]::new))
                    .application(application)
                    .build();

            // Process registration
            userRepository.saveAndFlush(user);

            return collectPrivileges.stream().map(Privilege::getName).collect(Collectors.toList());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException | IllegalArgumentException | IllegalBlockSizeException e) {
            throw new ContractValidationException(INVALID_PASSWORD_CIPHER.message, INVALID_PASSWORD_CIPHER.code);
        }
    }

    @Override
    public List<String> authenticate(final String login, final String appName, final String encipheringPassword) throws ContractValidationException, IOException {
        // Check if user exist for the target app name
        final User user = Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .orElseThrow(() -> new ContractValidationException(USER_NOT_FOUND.message, USER_NOT_FOUND.code));

        try {
            // Process to deciphering the password
            final String decipherPassword = KeyPairGenerator.decipher(KeyValueStore.load(privateKeyPath, appName), encipheringPassword);

            // Check if the password is correct
            if (!passwordEncoder.matches(decipherPassword, user.getPassword())) {
                throw new ContractValidationException(PASSWORD_INCORRECT.message, PASSWORD_INCORRECT.code);
            }

            // Return privileges
            return user.getPrivileges().stream().map(Privilege::getName).collect(Collectors.toList());
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException |
                InvalidKeyException | BadPaddingException | IllegalArgumentException | IllegalBlockSizeException e) {
            throw new ContractValidationException(INVALID_PASSWORD_CIPHER.message, INVALID_PASSWORD_CIPHER.code);
        }
    }

    @Override
    public void setPassword(final String login, final String appName, final String password) throws ContractValidationException {
        // Validate password
        if (!InputValidator.getInstance(RegexValidator.PASSWORD).validate(password)) {
            throw new ContractValidationException(INVALID_PASSWORD.message, INVALID_PASSWORD.code);
        }

        // Check if user exist
        final User user = Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                                  .orElseThrow(() -> new ContractValidationException(USER_NOT_FOUND.message, USER_NOT_FOUND.code));

        // check if it is not the same
        if (passwordEncoder.matches(password, user.getPassword())) {
            throw new ContractValidationException(SAME_PASSWORD.message, SAME_PASSWORD.code);
        }

        // Process updating password
        user.setPassword(password);
        userRepository.flush();
    }

    @Override
    public void unsubscribe(final String login, final String appName) {
        // Detach user privileges after deleted
        Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .ifPresent(u -> u.removePrivileges(u.getPrivileges().toArray(Privilege[]::new)));

        // Process deleting user
        userRepository.deleteUserByLoginAndApplication_Name(login, appName);
    }

    @Override
    public void addPrivileges(final String login, final String appName, String... privileges) throws ContractValidationException {
        // Check if user exist
        final User user = Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .orElseThrow(() -> new ContractValidationException(USER_NOT_FOUND.message, USER_NOT_FOUND.code));

        final List<Privilege> collectPrivileges = Arrays.stream(privileges)
                .map(privilege -> privilegeRepository.findPrivilegeByNameAndApplication_Name(privilege, appName))
                .collect(Collectors.toList());
        // Check if privilege(s) available for this login
        if (collectPrivileges.isEmpty() || collectPrivileges.contains(null)) {
            throw new ContractValidationException(PRIV_NOT_FOUND.message, PRIV_NOT_FOUND.code);
        }
        // Check if privilege(s) already granted for this user
        final Stream<Privilege> existingPrivileges = user.getPrivileges().stream().filter(collectPrivileges::contains);
        if (existingPrivileges.findAny().isPresent()) {
            throw new ContractValidationException(PRIV_EXIST_USER.message + existingPrivileges.toString(), PRIV_EXIST_USER.code);
        }

        // Process adding privilege(s)
        user.addPrivileges(collectPrivileges.stream().filter(Objects::nonNull).toArray(Privilege[]::new));
        userRepository.flush();
    }

    @Override
    public void removePrivileges(final String login, final String appName, final String... privileges) throws ContractValidationException {
        // Check if user exist
        final User user = Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .orElseThrow(() -> new ContractValidationException(USER_NOT_FOUND.message, USER_NOT_FOUND.code));

        // Check if privileges not containing default privilege
        if (Optional.ofNullable(applicationRepository.findApplicationByName(appName))
                .map(Application::getDefaultPrivilege)
                .filter(privilege -> Arrays.asList(privileges).contains(privilege))
                .isPresent()) {
            throw new ContractValidationException(DEF_PRIV_NOT_DELETED.message, DEF_PRIV_NOT_DELETED.code);
        }

        final List<Privilege> collectPrivileges = Arrays.stream(privileges)
                .map(privilege -> privilegeRepository.findUserPrivilege(privilege, appName, login))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
        // Process deleting privilege(s)
        user.removePrivileges(collectPrivileges.toArray(Privilege[]::new));
        userRepository.flush();
    }

    @Override
    public String getKey(final String login, final String appName) throws ContractValidationException {
        return Optional.ofNullable(userRepository.findUserByLoginAndApplication_Name(login, appName))
                .map(User::getSecretKey)
                .orElseThrow(() -> new ContractValidationException(USER_NOT_FOUND.message, USER_NOT_FOUND.code));
    }
}
