package fr.jihadoussad.heimdall.server.provider;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.io.Encoders;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.crypto.Mac;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.INVALID_TOKEN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
public class AuthorizationServiceTest {

    @TestConfiguration
    static class AuthenticationUserServiceContextConfiguration {
        @Bean
        public AuthorizationService authenticationUserService() {
            return new AuthorizationServiceImpl(Jackson2ObjectMapperBuilder.json().build());
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private AuthorizationService authorizationService;

    private String jwt;
    private Key secretKey;
    private final ObjectMapper objectMapper = Jackson2ObjectMapperBuilder.json().build();

    private static final String LOGIN = "test";
    private static final String PRIVILEGE = "maintainerTest";
    private static final String APP_NAME = "app";

    /*
        On configure ici un jeton jwt généré par l'AuthorizationService à 1 seconde d'expiration,
        un id généré aléatoirement, et une clé secrète Hmac_SHA256 généré par notre SecretKeyGenerator.
    */

    @BeforeEach
    public void setUp() throws NoSuchAlgorithmException {
        this.secretKey = SecretKeyGenerator.createHashmacSHA256Key();
        final long expiration = 1000; // 1 seconde

        jwt = authorizationService.generateToken(secretKey, expiration, LOGIN, APP_NAME, PRIVILEGE);
    }

    /*
        Nous vérifions dans ce test si nous arrivons à générer correctement un jeton et
        qu'à partir de la clé secrète, nous récupérons bien nos informations dans le payload.
    */

    @Test
    public void generateTokenTest() throws NoSuchAlgorithmException, InvalidKeyException {
        final String[] jwtArray = jwt.split("\\.");
        final String header = jwtArray[0];
        final String payload = jwtArray[1];
        final String feeder = header+"."+payload;

        final Mac sha256HMAC = Mac.getInstance("HmacSHA256");
        sha256HMAC.init(secretKey);
        final String signatureExpected = Encoders.BASE64URL.encode(sha256HMAC.doFinal(feeder.getBytes()));

        assertThat(signatureExpected).isEqualTo(jwtArray[2]);
    }

    @Test
    public void getTokenPayloadTest() throws InvalidTokenException {
        assertThat(authorizationService.getPayload(jwt))
                .isNotNull()
                .isNotEmpty()
                .containsEntry("sub", LOGIN)
                .containsEntry("appName", APP_NAME);
    }

    @Test
    public void getInvalidTokenPayloadTest() throws InvalidTokenException {
        final String code = assertThrows(InvalidTokenException.class, () -> authorizationService.getPayload("jwt")).getCode();

        assertThat(code).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void verifyTokenTest() throws InvalidTokenException {
        final Map<String, Object> jws = authorizationService.verifyToken(jwt, secretKey);

        assertThat(jws.get("sub")).isEqualTo(LOGIN);
        assertThat(jws.get("appName")).isEqualTo(APP_NAME);
        assertThat(jws.get("privileges")).isEqualTo(Collections.singletonList(PRIVILEGE));
    }

    /*
        Ce test nous permet de vérifier si l'on peut casser le jeton via la faille "Signature Stripping".
        Celle-ci consiste à decoder le header, de positionner l'algo en non-signé, et de renvoyer le jeton sans la signature
        tout en altérant le payload si on le souhaite.
    */

    @Test
    public void signatureStripingTest() throws JsonProcessingException, InvalidTokenException {
        final Map<String, String> header = new HashMap<>();
        header.put("alg", "none");

        final Map<String, Object> payload = authorizationService.getPayload(jwt);
        payload.put("privileges", "adminTest");

        final String alterJwt = Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(header))+ "."
                + Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(payload)) + ".";

        final String code = assertThrows(InvalidTokenException.class, () ->  authorizationService.verifyToken(alterJwt, secretKey)).getCode();
        assertThat(code).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void alterTokenTest() throws ExpiredJwtException, JsonProcessingException, InvalidTokenException {
        final String[] arrayJwt = jwt.split("\\.");

        final Map<String, Object> payload = authorizationService.getPayload(jwt);
        payload.put("privileges", "adminTest");

        final String alterJwt = arrayJwt[0] + "." + Base64.getEncoder().encodeToString(objectMapper.writeValueAsBytes(payload)) + "."
                + arrayJwt[2];

        final String code = assertThrows(InvalidTokenException.class, () ->  authorizationService.verifyToken(alterJwt, secretKey)).getCode();
        assertThat(code).isEqualTo(INVALID_TOKEN.code);
    }

    @Test
    public void tokenExpireTest() throws InterruptedException, ExpiredJwtException {
        Thread.sleep(1000);

        assertThrows(ExpiredJwtException.class, () ->  authorizationService.verifyToken(jwt, secretKey));
    }
}
