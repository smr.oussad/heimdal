package fr.jihadoussad.heimdall.server.provider;

import fr.jihadoussad.heimdall.server.contract.AuthenticationApplicationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.server.provider.factories.ApplicationFactory;
import fr.jihadoussad.heimdall.server.provider.factories.PrivilegeFactory;
import fr.jihadoussad.heimdall.server.provider.factories.UserFactory;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AuthenticationApplicationServiceImplTest {

    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @TestConfiguration
    static class AuthenticationUserServiceContextConfiguration {
        @Bean
        public AuthenticationApplicationService authenticationUserService() {
            return new AuthenticationApplicationServiceImpl(PASSWORD_ENCODER);
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private AuthenticationApplicationService authenticationApplicationService;

    @MockBean
    private ApplicationRepository applicationRepository;

    @MockBean
    private PrivilegeRepository privilegeRepository;

    @MockBean
    private UserRepository userRepository;

    private static final String LOGIN = "thor";
    private static final String PASSWORD = "Asggard!";
    private static final String SECRET_KEY = "mySecret";
    private static final String APP_NAME = "Asgard";
    private static final String DEFAULT_PRIVILEGE = "default";
    private static final Long EXPIRATION_TOKEN = 1000L;
    private static final String PRIVILEGE = "king";

    @BeforeAll
    public static void loadPrivateKeys() throws IOException {
        KeyValueStore.save("target/heimdall-key.priv", "Avengers", "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCYdGrTf5VUiWmvTqLlIRtumreOaOJ73z82s08n6lmk4Ab0gWhHfnCLd4BUhtmEfBOK5tNeX1yTeseCwHONnl6be5gm1pLIm/8raTgeFUJvIRqh1UDGN2K2WgWs5ir1YIZuJE9tCyrOobZhcceISowz6wISFGiqzfLKT30IfU3/2GuKpPMS+LTrfFIY1B5oHoX3LHWN0WrllnBeql7qvLs+zLCS8CMAKmEX47cVM6E9pOhB5qiBFd+02CDrnYfykfTJPCxQ1dqBY99AU3EPE+NatMIDyyI502IO7ph5bbAEGLHOg3WwsdumzamkgQZuSvJYW67DpoB3z2rKzSmEM2vxAgMBAAECggEAPClEtq2UOkX3pKx9b7n+30jCwrn1Fjjk8ysTQ4chVQTq+bXPdtcYhpDj3XdRgwyLkbbBFm6/U5uR+7ECaJJkdyrwwfLO/cdBjNn6wTRWgRApOE+1PgNBOwCPWMmDjk1G/1Po2/kaLWx3UkDaIph4d0x4BrZzD2QyOU/g4WbBTwJ1UCTTKsae9Q7Z4qC1mjUMNwZUzqZ/dCwaIeKyZ4V1l/bfBSKmpdmv7eUz7Rn51/yUPB5g3HfDlSoZr1fsVxnfsbDr+PoEmYN4DN4qAL0CSCZXfzRIS+vXGk2gvCu2jocF7ytIXP3s44tx+5eCXghWt1mJUS85Gj0qPsoBxOh/WQKBgQDgS3/RpmWRIU3Ya2RCPlCTYTBxULHBGlc2m4oEV4iqWtaq7k7Uel5PrUP70ZdzOl46l3KMFi9ZcMDzJnAEh9SLXbssREfSc6aMvKiqOr77bZBD0Nxqi11n+wFvHchNBZ0klM6xs4SiUVHds1bEB5QKbYC14xu+oKzkG/GfWSZabwKBgQCuAUHN0t61hYGdb81DBX1ZJ8lqhasOPT6mA7J+yEMAgQJThqPsELZLB/FMal/T9HGTvoCS22IPRkV5+dKqkOg5xZ2SNtKDyr6X+uKEer0MxxDlpRENFdx6Q7XHKjoySkVrySVbolz5971ZuH187UUzvpX90b9+nSA5jYk8iJdPnwKBgHlmY/iFp9O1wvywo8N3FwWuRRoQIyXxq+LSrRFMj5wlLqu18NTwprtZfMJ/3wlvPjFYZ5eLKnWuocD5vQe2vUPxp61+B6HRFwR12JTK1zQfSUZrdeH1LMlrAouyAwgtUbDLGlT0ZYW1ninxN7VCpM5AFrpcAlhbfzi4Jz+ocSnhAoGBAKXequTSU9yDxLfYYLm33PKFG9qASzxKo5LLyXZT3pL3Z8lJeq9Iiw1hJ+MpL+ozhaVKHvRfTs2ytf39aJWRht2zhG5h9jENrrvfu9h/zAEHLhNLU8K+iSq/quONXGaeKCkEWKlOUAU05PpHBNB2udYGoqKPNFU04Qgb5KugpWSPAoGADg2E9RdTKV+4KyMymx/SjjJOxiU+qV5sSi5CJgMIhk9yMtd8wBLR26H9S10dvbQo3SueQ8KMwvYBXtt+S9hRkd8RkkJyaeCk6ChSlss9PMrTAZxJYBanpNZOODvg2+GI2mWSYW/TPjaGiuVEtIrKheTGnUAxBW9GfrPzZjBbuvY=");
    }

    @BeforeEach
    public void setUp() {
        final Application heimdall = ApplicationFactory
                .getInstance()
                .name("heimdall")
                .defaultPrivilege(DEFAULT_PRIVILEGE)
                .expirationToken(EXPIRATION_TOKEN)
                .build();
        when(applicationRepository.findApplicationByName("heimdall"))
                .thenReturn(heimdall);

        final Privilege defaultPrivilege = PrivilegeFactory
                .getInstance()
                .name(DEFAULT_PRIVILEGE)
                .application(heimdall)
                .build();
        when(privilegeRepository.findPrivilegeByNameAndApplication_Name(DEFAULT_PRIVILEGE, "heimdall"))
                .thenReturn(defaultPrivilege);

        final Application asgard = ApplicationFactory
                .getInstance()
                .name(APP_NAME)
                .defaultPrivilege(DEFAULT_PRIVILEGE)
                .expirationToken(EXPIRATION_TOKEN)
                .build();
        when(applicationRepository.findApplicationByName(APP_NAME))
                .thenReturn(asgard);

        final Privilege privilege = PrivilegeFactory
                .getInstance()
                .name(PRIVILEGE)
                .application(asgard)
                .build();
        when(privilegeRepository.findPrivilegeByNameAndApplication_Name(PRIVILEGE, APP_NAME))
                .thenReturn(privilege);
        when(privilegeRepository.findUserPrivilege(PRIVILEGE, APP_NAME, LOGIN))
                .thenReturn(privilege);

        final User thor = UserFactory
                .getInstance()
                .login(LOGIN)
                .password(PASSWORD_ENCODER.encode(PASSWORD))
                .privileges(privilege)
                .application(asgard)
                .build();
        when(userRepository.findUserByLoginAndApplication_Name(LOGIN, APP_NAME))
                .thenReturn(thor);

        ReflectionTestUtils.setField(authenticationApplicationService, "privateKeyPath", "target/heimdall-key.priv");
    }

    @Test
    public void banUserTest() {
        authenticationApplicationService.banUser(LOGIN, APP_NAME);
    }

    @Test
    public void banUserWithLoginNotExistTest() {
        authenticationApplicationService.banUser("foo", APP_NAME);
    }

    @Test
    public void banUserWithApplicationNotExistTest() {
        authenticationApplicationService.banUser(LOGIN, "fooApp");
    }

    @Test
    public void registerApplicationWhenNameInvalidTest() {
        final String code = assertThrows(ContractValidationException.class, () ->
                authenticationApplicationService.registerApplication("Foo", "p@ssw0rd", DEFAULT_PRIVILEGE, SECRET_KEY, 1000L)).getCode();
        assertThat(code).isEqualTo(INVALID_LOGIN.code);
    }

    @Test
    public void registerApplicationExistingTest() {
        final String code = assertThrows(ContractValidationException.class, () ->
                authenticationApplicationService.registerApplication(APP_NAME, "p@ssw0rd", DEFAULT_PRIVILEGE, SECRET_KEY, 1000L)).getCode();
        assertThat(code).isEqualTo(APP_EXIST.code);
    }

    @Test
    public void registerApplicationWhenPasswordInvalidTest() {
        final String code = assertThrows(ContractValidationException.class, () ->
                authenticationApplicationService.registerApplication("FooApp", "password", DEFAULT_PRIVILEGE, SECRET_KEY, 1000L)).getCode();
        assertThat(code).isEqualTo(INVALID_PASSWORD.code);
    }

    @Test
    public void registerApplicationTest() throws ContractValidationException, IOException, NoSuchAlgorithmException {
        authenticationApplicationService.registerApplication("FooApp", "p@ssw0rd",DEFAULT_PRIVILEGE, SECRET_KEY, 1000L);
    }

    @Test
    public void unsubscribeApplicationTest() {
        authenticationApplicationService.unsubscribeApplication(APP_NAME);
    }

    @Test
    public void addPrivilegesWhenApplicationNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationApplicationService.addPrivileges("Foo")).getCode();
        assertThat(code).isEqualTo(APP_NOT_FOUND.code);
    }

    @Test
    public void addExistingPrivilegesTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationApplicationService.addPrivileges(APP_NAME, PRIVILEGE)).getCode();
        assertThat(code).isEqualTo(PRIV_EXIST_APP.code);
    }

    @Test
    public void addPrivilegesTest() throws ContractValidationException {
        authenticationApplicationService.addPrivileges(APP_NAME, "GUEST");
    }

    @Test
    public void removePrivilegesWhenApplicationNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationApplicationService.removePrivileges("Foo", DEFAULT_PRIVILEGE)).getCode();
        assertThat(code).isEqualTo(APP_NOT_FOUND.code);
    }

    @Test
    public void removePrivilegesWhenDefaultFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationApplicationService.removePrivileges(APP_NAME, DEFAULT_PRIVILEGE)).getCode();
        assertThat(code).isEqualTo(DEF_PRIV_NOT_DELETED.code);
    }

    @Test
    public void removePrivilegesTest() throws ContractValidationException {
        authenticationApplicationService.removePrivileges(APP_NAME, PRIVILEGE);
    }

    @Test
    public void getExpirationTokenWhenAppNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationApplicationService.getExpirationToken("foo")).getCode();
        assertThat(code).isEqualTo(APP_NOT_FOUND.code);
    }

    @Test
    public void getExpirationTokenTest() throws ContractValidationException {
        assertThat(authenticationApplicationService.getExpirationToken(APP_NAME))
                .isEqualTo(EXPIRATION_TOKEN);
    }
}