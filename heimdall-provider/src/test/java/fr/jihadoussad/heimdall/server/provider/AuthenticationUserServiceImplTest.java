package fr.jihadoussad.heimdall.server.provider;

import fr.jihadoussad.heimdall.server.contract.AuthenticationUserService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import fr.jihadoussad.heimdall.server.core.repositories.ApplicationRepository;
import fr.jihadoussad.heimdall.server.core.repositories.PrivilegeRepository;
import fr.jihadoussad.heimdall.server.core.repositories.UserRepository;
import fr.jihadoussad.heimdall.server.provider.factories.ApplicationFactory;
import fr.jihadoussad.heimdall.server.provider.factories.PrivilegeFactory;
import fr.jihadoussad.heimdall.server.provider.factories.UserFactory;
import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import fr.jihadoussad.heimdall.utils.store.KeyValueStore;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.util.ReflectionTestUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class AuthenticationUserServiceImplTest {

    private static final PasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @TestConfiguration
    static class AuthenticationUserServiceContextConfiguration {

        @Bean
        public AuthenticationUserService authenticationUserService() {
            return new AuthenticationUserServiceImpl(PASSWORD_ENCODER);
        }
    }

    @SuppressWarnings("SpringJavaAutowiredMembersInspection")
    @Autowired
    private AuthenticationUserService authenticationUserService;

    @MockBean
    private ApplicationRepository applicationRepository;

    @MockBean
    private PrivilegeRepository privilegeRepository;

    @MockBean
    private UserRepository userRepository;

    private static final String LOGIN = "thor";
    private static final String PASSWORD = "Asgqard!";
    private static final String SECRET_KEY = "mySecret";
    private static final String APP_NAME = "Avengers";
    private static final String DEFAULT_PRIVILEGE = "default";
    private static final String PRIVILEGE_NAME = "member";
    private static final Long EXPIRATION_TOKEN = 1000L;
    private static final String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmHRq03+VVIlpr06i5SEbbpq3jmjie98/NrNPJ+pZpOAG9IFoR35wi3eAVIbZhHwTiubTXl9ck3rHgsBzjZ5em3uYJtaSyJv/K2k4HhVCbyEaodVAxjditloFrOYq9WCGbiRPbQsqzqG2YXHHiEqMM+sCEhRoqs3yyk99CH1N/9hriqTzEvi063xSGNQeaB6F9yx1jdFq5ZZwXqpe6ry7PsywkvAjACphF+O3FTOhPaToQeaogRXftNgg652H8pH0yTwsUNXagWPfQFNxDxPjWrTCA8siOdNiDu6YeW2wBBixzoN1sLHbps2ppIEGbkryWFuuw6aAd89qys0phDNr8QIDAQAB";

    @BeforeAll
    public static void loadPrivateKeys() throws IOException {
        KeyValueStore.save("target/heimdall-key.priv", "Avengers", "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCYdGrTf5VUiWmvTqLlIRtumreOaOJ73z82s08n6lmk4Ab0gWhHfnCLd4BUhtmEfBOK5tNeX1yTeseCwHONnl6be5gm1pLIm/8raTgeFUJvIRqh1UDGN2K2WgWs5ir1YIZuJE9tCyrOobZhcceISowz6wISFGiqzfLKT30IfU3/2GuKpPMS+LTrfFIY1B5oHoX3LHWN0WrllnBeql7qvLs+zLCS8CMAKmEX47cVM6E9pOhB5qiBFd+02CDrnYfykfTJPCxQ1dqBY99AU3EPE+NatMIDyyI502IO7ph5bbAEGLHOg3WwsdumzamkgQZuSvJYW67DpoB3z2rKzSmEM2vxAgMBAAECggEAPClEtq2UOkX3pKx9b7n+30jCwrn1Fjjk8ysTQ4chVQTq+bXPdtcYhpDj3XdRgwyLkbbBFm6/U5uR+7ECaJJkdyrwwfLO/cdBjNn6wTRWgRApOE+1PgNBOwCPWMmDjk1G/1Po2/kaLWx3UkDaIph4d0x4BrZzD2QyOU/g4WbBTwJ1UCTTKsae9Q7Z4qC1mjUMNwZUzqZ/dCwaIeKyZ4V1l/bfBSKmpdmv7eUz7Rn51/yUPB5g3HfDlSoZr1fsVxnfsbDr+PoEmYN4DN4qAL0CSCZXfzRIS+vXGk2gvCu2jocF7ytIXP3s44tx+5eCXghWt1mJUS85Gj0qPsoBxOh/WQKBgQDgS3/RpmWRIU3Ya2RCPlCTYTBxULHBGlc2m4oEV4iqWtaq7k7Uel5PrUP70ZdzOl46l3KMFi9ZcMDzJnAEh9SLXbssREfSc6aMvKiqOr77bZBD0Nxqi11n+wFvHchNBZ0klM6xs4SiUVHds1bEB5QKbYC14xu+oKzkG/GfWSZabwKBgQCuAUHN0t61hYGdb81DBX1ZJ8lqhasOPT6mA7J+yEMAgQJThqPsELZLB/FMal/T9HGTvoCS22IPRkV5+dKqkOg5xZ2SNtKDyr6X+uKEer0MxxDlpRENFdx6Q7XHKjoySkVrySVbolz5971ZuH187UUzvpX90b9+nSA5jYk8iJdPnwKBgHlmY/iFp9O1wvywo8N3FwWuRRoQIyXxq+LSrRFMj5wlLqu18NTwprtZfMJ/3wlvPjFYZ5eLKnWuocD5vQe2vUPxp61+B6HRFwR12JTK1zQfSUZrdeH1LMlrAouyAwgtUbDLGlT0ZYW1ninxN7VCpM5AFrpcAlhbfzi4Jz+ocSnhAoGBAKXequTSU9yDxLfYYLm33PKFG9qASzxKo5LLyXZT3pL3Z8lJeq9Iiw1hJ+MpL+ozhaVKHvRfTs2ytf39aJWRht2zhG5h9jENrrvfu9h/zAEHLhNLU8K+iSq/quONXGaeKCkEWKlOUAU05PpHBNB2udYGoqKPNFU04Qgb5KugpWSPAoGADg2E9RdTKV+4KyMymx/SjjJOxiU+qV5sSi5CJgMIhk9yMtd8wBLR26H9S10dvbQo3SueQ8KMwvYBXtt+S9hRkd8RkkJyaeCk6ChSlss9PMrTAZxJYBanpNZOODvg2+GI2mWSYW/TPjaGiuVEtIrKheTGnUAxBW9GfrPzZjBbuvY=");
    }

    @BeforeEach
    public void setUp() {
        final Application avengers = ApplicationFactory
                .getInstance()
                .name(APP_NAME)
                .defaultPrivilege(DEFAULT_PRIVILEGE)
                .expirationToken(EXPIRATION_TOKEN)
                .build();
        when(applicationRepository.findApplicationByName(APP_NAME))
                .thenReturn(avengers);
        final Privilege privilege = PrivilegeFactory
                .getInstance()
                .name(PRIVILEGE_NAME)
                .application(avengers)
                .build();
        when(privilegeRepository.findPrivilegeByNameAndApplication_Name(PRIVILEGE_NAME, APP_NAME))
                .thenReturn(privilege);
        when(privilegeRepository.findUserPrivilege(PRIVILEGE_NAME, APP_NAME, LOGIN))
                .thenReturn(privilege);
        final Privilege defaultPrivilege = PrivilegeFactory
                .getInstance()
                .name(DEFAULT_PRIVILEGE)
                .application(avengers)
                .build();
        when(privilegeRepository.findPrivilegeByNameAndApplication_Name(DEFAULT_PRIVILEGE, APP_NAME))
                .thenReturn(defaultPrivilege);
        when(privilegeRepository.findUserPrivilege(DEFAULT_PRIVILEGE, APP_NAME, LOGIN))
                .thenReturn(defaultPrivilege);
        final User thor = UserFactory
                .getInstance()
                .login(LOGIN)
                .password(PASSWORD_ENCODER.encode(PASSWORD))
                .secretKey(SECRET_KEY)
                .application(avengers)
                .privileges(defaultPrivilege, privilege)
                .build();
        when(userRepository.findUserByLoginAndApplication_Name(LOGIN, APP_NAME))
                .thenReturn(thor);

        ReflectionTestUtils.setField(authenticationUserService, "privateKeyPath", "target/heimdall-key.priv");
    }

    @Test
    public void registerUserWithLoginInvalidTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.registerUser("foo", "foo", SECRET_KEY, "foo")).getCode();
        assertThat(code).isEqualTo(INVALID_LOGIN.code);
    }

    @Test
    public void registerUserExistingTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.registerUser(LOGIN, "foo", SECRET_KEY, APP_NAME)).getCode();
        assertThat(code).isEqualTo(USER_EXIST.code);
    }

    @Test
    public void registerUserWithPasswordInvalidTest() {
        final String code = assertThrows(ContractValidationException.class,
                () -> authenticationUserService.registerUser("login",  KeyPairGenerator.cipher(publicKey, "foo"), SECRET_KEY, APP_NAME)).getCode();
        assertThat(code).isEqualTo(INVALID_PASSWORD.code);
    }

    @Test
    public void registerUserWhenAppNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.registerUser("login", "P@ssword",SECRET_KEY, "FOO")).getCode();
        assertThat(code).isEqualTo(APP_NOT_FOUND.code);
    }

    @Test
    public void registerUserWithPrivilegeNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class,
                () -> authenticationUserService.registerUser("login", KeyPairGenerator.cipher(publicKey, "P@ssword"), SECRET_KEY, APP_NAME, "foo")).getCode();
        assertThat(code).isEqualTo(PRIV_NOT_FOUND.code);
    }

    @Test
    public void registerUserWithEncipheringPasswordTest()  {
        final String code = assertThrows(ContractValidationException.class,
                () -> authenticationUserService.registerUser("login", "P@ssword", SECRET_KEY, APP_NAME)).getCode();
        assertThat(code).isEqualTo(INVALID_PASSWORD_CIPHER.code);
    }

    @Test
    public void registerUserWithEmptyPrivilegeTest() throws ContractValidationException, IOException, NoSuchPaddingException, NoSuchAlgorithmException,
            IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        authenticationUserService.registerUser("login", KeyPairGenerator.cipher(publicKey, "P@ssword"), SECRET_KEY, APP_NAME);
    }

    @Test
    public void registerUserWithDefaultPrivilegeTest() throws ContractValidationException, IOException, NoSuchPaddingException, NoSuchAlgorithmException,
            IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        authenticationUserService.registerUser("login", KeyPairGenerator.cipher(publicKey, "P@ssword"), SECRET_KEY, APP_NAME, DEFAULT_PRIVILEGE);
    }

    @Test
    public void registerUserTest() throws ContractValidationException, IOException, NoSuchPaddingException, NoSuchAlgorithmException,
            IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        authenticationUserService.registerUser("login", KeyPairGenerator.cipher(publicKey, "P@ssword"), SECRET_KEY, APP_NAME, PRIVILEGE_NAME);
    }

    @Test
    public void authenticateWhenUserNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.authenticate("foo", APP_NAME, "foo")).getCode();
        assertThat(code).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void authenticateWhenPasswordIncorrectTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.authenticate(LOGIN, APP_NAME, KeyPairGenerator.cipher(publicKey, "foo"))).getCode();
        assertThat(code).isEqualTo(PASSWORD_INCORRECT.code);
    }

    @Test
    public void authenticateWhenPasswordEncipheredTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.authenticate(LOGIN, APP_NAME, "foo")).getCode();
        assertThat(code).isEqualTo(INVALID_PASSWORD_CIPHER.code);
    }

    @Test
    public void authenticateTest() throws ContractValidationException, IOException, NoSuchPaddingException, NoSuchAlgorithmException,
            IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        final List<String> privileges = authenticationUserService.authenticate(LOGIN, APP_NAME, KeyPairGenerator.cipher(publicKey, PASSWORD));
        assertThat(privileges).containsExactly(DEFAULT_PRIVILEGE, PRIVILEGE_NAME);
    }

    @Test
    public void setPasswordWhenUserNotFound() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.setPassword("foo", APP_NAME, "Avengers!")).getCode();
        assertThat(code).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void setPasswordWhenPasswordInvalid() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.setPassword(LOGIN, APP_NAME, "foo")).getCode();
        assertThat(code).isEqualTo(INVALID_PASSWORD.code);
    }

    @Test
    public void setPasswordWhenSamePasswordInvalid() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.setPassword(LOGIN, APP_NAME, PASSWORD)).getCode();
        assertThat(code).isEqualTo(SAME_PASSWORD.code);
    }

     @Test
    public void setPasswordTest() throws ContractValidationException {
        authenticationUserService.setPassword(LOGIN, APP_NAME,"Avengers!");
    }

    @Test
    public void unsubscribeTest() {
        authenticationUserService.unsubscribe(LOGIN, APP_NAME);
    }

    @Test
    public void unsubscribeWithLoginNotExistTest() {
        authenticationUserService.unsubscribe("foo", APP_NAME);
    }

    @Test
    public void unsubscribeWithApplicationNotExistTest() {
        authenticationUserService.unsubscribe(LOGIN, "fooApp");
    }

    @Test
    public void addUserPrivilegesWhenUserNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.addPrivileges("foo", APP_NAME)).getCode();
        assertThat(code).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void addUserWithEmptyPrivilegesTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.addPrivileges(LOGIN, APP_NAME)).getCode();
        assertThat(code).isEqualTo(PRIV_NOT_FOUND.code);
    }

    @Test
    public void addUserUnavailablePrivilegesTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.addPrivileges(LOGIN, APP_NAME, "Foo")).getCode();
        assertThat(code).isEqualTo(PRIV_NOT_FOUND.code);
    }

    @Test
    public void addUserExistingPrivilegesTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.addPrivileges(LOGIN, APP_NAME, PRIVILEGE_NAME)).getCode();
        assertThat(code).isEqualTo(PRIV_EXIST_USER.code);
    }

    @Test
    public void addUserPrivilegesTest() throws ContractValidationException {
        when(privilegeRepository.findPrivilegeByNameAndApplication_Name("FOO", APP_NAME))
                .thenReturn(PrivilegeFactory.getInstance().name("FOO").build());
        authenticationUserService.addPrivileges(LOGIN, APP_NAME, "FOO");
    }

    @Test
    public void removeUserPrivilegesWhenUserNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.removePrivileges("foo", APP_NAME)).getCode();
        assertThat(code).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void removeUserPrivilegesWhenDefaultPrivilegeFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.removePrivileges(LOGIN, APP_NAME, DEFAULT_PRIVILEGE)).getCode();
        assertThat(code).isEqualTo(DEF_PRIV_NOT_DELETED.code);
    }

    @Test
    public void removeUserPrivilegesTest() throws ContractValidationException {
        authenticationUserService.removePrivileges(LOGIN, APP_NAME, PRIVILEGE_NAME);
    }

    @Test
    public void getSecretKeyWhenUserNotFoundTest() {
        final String code = assertThrows(ContractValidationException.class, () -> authenticationUserService.getKey("foo", "foo")).getCode();
        assertThat(code).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    public void getUserSecretKeyTest() throws ContractValidationException {
        authenticationUserService.getKey(LOGIN, APP_NAME);
    }
}