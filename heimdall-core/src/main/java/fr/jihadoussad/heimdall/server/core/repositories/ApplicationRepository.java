package fr.jihadoussad.heimdall.server.core.repositories;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ApplicationRepository extends JpaRepository<Application, String> {

    Application findApplicationByName(final String name);

    @Query(value = "select a from Application a left join a.users u where u.login=?1")
    List<Application> findApplicationByUserLogin(final String login);

    void deleteApplicationByName(final String name);
}
