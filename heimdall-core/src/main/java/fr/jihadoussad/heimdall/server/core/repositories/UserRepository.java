package fr.jihadoussad.heimdall.server.core.repositories;


import fr.jihadoussad.heimdall.server.core.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByLoginAndApplication_Name(final String login, final String appName);

    void deleteUserByLoginAndApplication_Name(final String login, final String appName);
}
