package fr.jihadoussad.heimdall.server.core.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
public class Application {

    @Id
    private String name;

    private String defaultPrivilege;

    private Long expirationToken;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", orphanRemoval = true, cascade = CascadeType.ALL)
    private final List<Privilege> privileges;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", orphanRemoval = true, cascade = CascadeType.ALL)
    private final List<User> users;

    public Application() {
        privileges = new ArrayList<>();
        users = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefaultPrivilege() {
        return defaultPrivilege;
    }

    public void setDefaultPrivilege(final String defaultPrivilege) {
        this.defaultPrivilege = defaultPrivilege;
    }

    public Long getExpirationToken() {
        return expirationToken;
    }

    public void setExpirationToken(final Long tokenExpiration) {
        this.expirationToken = tokenExpiration;
    }

    public List<User> getUsers() {
        return users;
    }

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void addUsers(final User... users) {
        this.users.addAll(Arrays.asList(users));
        Arrays.stream(users)
                .forEach(user -> user.setApplication(this));
    }

    public void removeUsers(final User... users) {
        Arrays.stream(users)
                .forEach(user -> {
                    user.setApplication(null);
                    user.getPrivileges().clear(); //Also remove the privileges
                });
        this.users.removeAll(Arrays.asList(users));

    }
}
