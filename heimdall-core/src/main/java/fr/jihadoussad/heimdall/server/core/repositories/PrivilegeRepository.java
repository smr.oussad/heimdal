package fr.jihadoussad.heimdall.server.core.repositories;

import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface PrivilegeRepository extends JpaRepository<Privilege, Long> {

    Privilege findPrivilegeByNameAndApplication_Name(final String privilegeName, final String appName);

    @Query("select p from Privilege p where p.name=?1 and p.application=(select a from Application a where a.name=?2) and (select u from User u where u.login=?3) member p.users")
    Privilege findUserPrivilege(final String privilegeName, final String applicationName, final String login);

    void deletePrivilegeByNameAndApplication_Name(final String privilegeName, final String appName);
}
