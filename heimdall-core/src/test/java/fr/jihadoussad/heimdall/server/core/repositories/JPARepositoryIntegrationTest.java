package fr.jihadoussad.heimdall.server.core.repositories;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
class JPARepositoryIntegrationTest {

    @Resource
    private ApplicationRepository applicationRepository;

    @Resource
    private PrivilegeRepository privilegeRepository;

    @Resource
    private UserRepository userRepository;

    @Test
    public void dataPersistenceApplicationTest() {
        assertThat(applicationRepository.findAll())
                .isNotEmpty()
                .hasSize(2);

        Application actual = applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME);
        assertThat(actual)
                .isNotNull();

        assertThat(actual.getUsers())
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .extracting(User::getLogin)
                .containsExactly(JPARepositoryApplication.LOGIN);

        assertThat(actual.getPrivileges())
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .extracting(Privilege::getName)
                .containsExactly(JPARepositoryApplication.DEFAULT_PRIVILEGE, JPARepositoryApplication.PRIVILEGE);

        assertThat(actual.getDefaultPrivilege())
                .isNotNull()
                .isEqualTo(JPARepositoryApplication.DEFAULT_PRIVILEGE);

        assertThat(actual.getExpirationToken())
                .isNotNull()
                .isEqualTo(JPARepositoryApplication.EXPIRATION_TOKEN);

        actual = applicationRepository.findApplicationByName("Foo");
        assertThat(actual)
                .isNotNull();

        assertThat(actual.getUsers())
                .isNullOrEmpty();

        assertThat(actual.getPrivileges())
                .isNotNull()
                .isNotEmpty()
                .hasSize(1)
                .extracting(Privilege::getName)
                .containsExactly(JPARepositoryApplication.DEFAULT_PRIVILEGE);

        assertThat(actual.getDefaultPrivilege())
                .isNotNull()
                .isEqualTo(JPARepositoryApplication.DEFAULT_PRIVILEGE);

        assertThat(actual.getExpirationToken())
                .isNotNull()
                .isEqualTo(JPARepositoryApplication.EXPIRATION_TOKEN);
    }

    @Test
    public void dataPersistencePrivilegeTest() {
        assertThat(privilegeRepository.findAll())
                .isNotEmpty()
                .hasSize(3)
                .extracting(Privilege::getName)
                .containsExactly(JPARepositoryApplication.DEFAULT_PRIVILEGE, JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.DEFAULT_PRIVILEGE);

        final Privilege actual = privilegeRepository
                .findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME);

        assertThat(actual.getApplication())
                .isNotNull()
                .extracting(Application::getName)
                .isEqualTo(JPARepositoryApplication.APP_NAME);

        assertThat(privilegeRepository.findUserPrivilege(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME, JPARepositoryApplication.LOGIN))
                .isNotNull()
                .extracting(Privilege::getName)
                .isEqualTo(JPARepositoryApplication.PRIVILEGE);
    }

    @Test
    public void dataPersistenceUserTest() {
        final User actual = userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME);

        assertThat(actual)
                .isNotNull();

        assertThat(actual.getApplication())
               .isNotNull()
               .extracting(Application::getName)
               .isEqualTo(JPARepositoryApplication.APP_NAME);

        assertThat(actual.getPrivileges())
                .isNotNull()
                .isNotEmpty()
                .hasSize(2)
                .extracting(Privilege::getName)
                .containsExactly(JPARepositoryApplication.DEFAULT_PRIVILEGE, JPARepositoryApplication.PRIVILEGE);
    }

    @Test
    public void dataRelationMappingWithApplicationAndAllTest() {
        applicationRepository.deleteApplicationByName(JPARepositoryApplication.APP_NAME);

        assertThat(applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME))
                .isNull();

        assertThat(privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME))
                .isNull();

        assertThat(userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME))
                .isNull();
    }

    @Test
    public void dataRelationMappingWithPrivilegeAndAllTest() {
        privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME)
                .removeUsers();
        privilegeRepository.flush();

        privilegeRepository.deletePrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME);

        assertThat(privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME))
                .isNull();

        assertThat(applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(Application::getPrivileges)
                .asList()
                .hasSize(1);

        assertThat(userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(User::getPrivileges)
                .asList()
                .hasSize(1);
    }

    @Test
    public void dataCheckPrivilegeIfUserLoginCanUseThisPrivilege() {
        assertThat(privilegeRepository.findUserPrivilege(JPARepositoryApplication.DEFAULT_PRIVILEGE, "Foo", JPARepositoryApplication.LOGIN))
                .isNull();
    }

    @Test
    public void dataDeleteRelationMappingWithUserAndAllTest() {
        // Detach user privileges after deleted
        final User thor = userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME);
        thor.removePrivileges(thor.getPrivileges().toArray(Privilege[]::new));
        userRepository.flush();

        userRepository.deleteUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME);

        assertThat(userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME))
                .isNull();

        assertThat(privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME))
                .isNotNull();

        assertThat(privilegeRepository.findUserPrivilege(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME, JPARepositoryApplication.LOGIN))
                .isNull();

        assertThat(applicationRepository.findApplicationByUserLogin(JPARepositoryApplication.LOGIN))
                .isNullOrEmpty();

        assertThat(applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(Application::getUsers)
                .asList()
                .isNullOrEmpty();
    }

    @Test
    public void dataUpdateRelationMappingWithUserAndAllTest() {
        final User thor = userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME);
        // Add Thor in foo application and remove on Avengers
        Application avengers = applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME);
        avengers.removeUsers(thor);
        final Application foo = applicationRepository.findApplicationByName("Foo");
        foo.addUsers(thor);
        // Set thor's password
        thor.setPassword("fOo");

        // Flushing
        userRepository.flush();
        applicationRepository.flush();

        assertThat(userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME))
                .isNull();

        final User actualUser = userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, "Foo");
        assertThat(actualUser)
                .isNotNull();
        assertThat(actualUser.getApplication())
                .isNotNull()
                .extracting(Application::getName)
                .isEqualTo("Foo");
        assertThat(actualUser.getPrivileges())
                .isNullOrEmpty();
        assertThat(actualUser.getPassword())
                .isEqualTo("fOo");

        Application actualApp = applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME);
        assertThat(actualApp.getUsers())
                .isNullOrEmpty();
        assertThat(actualApp.getPrivileges())
                .isNotNull()
                .isNotEmpty()
                .extracting(Privilege::getName)
                .containsExactly(JPARepositoryApplication.DEFAULT_PRIVILEGE, JPARepositoryApplication.PRIVILEGE);

        actualApp = applicationRepository.findApplicationByName("Foo");
        assertThat(actualApp.getUsers())
                .isNotNull()
                .isNotEmpty()
                .extracting(User::getLogin)
                .containsExactly(JPARepositoryApplication.LOGIN);
    }

    @Test
    public void removePrivilegeUserTest() {
        final User thor = userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME);
        final Privilege member = privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME);
        // remove member privilege for thor
        thor.removePrivileges(member);
        // Flushing
        userRepository.flush();

        assertThat(userRepository.findUserByLoginAndApplication_Name(JPARepositoryApplication.LOGIN, JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(User::getPrivileges)
                .asList()
                .hasSize(1);

        assertThat(applicationRepository.findApplicationByName(JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(Application::getUsers)
                .asList()
                .containsOnly(thor);

        assertThat(privilegeRepository.findPrivilegeByNameAndApplication_Name(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME))
                .isNotNull()
                .extracting(Privilege::getUsers)
                .asList()
                .isNullOrEmpty();

        assertThat(privilegeRepository.findUserPrivilege(JPARepositoryApplication.PRIVILEGE, JPARepositoryApplication.APP_NAME, JPARepositoryApplication.LOGIN))
                .isNull();
    }
}