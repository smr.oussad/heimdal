package fr.jihadoussad.heimdall.server.core.repositories;

import fr.jihadoussad.heimdall.server.core.entities.Application;
import fr.jihadoussad.heimdall.server.core.entities.Privilege;
import fr.jihadoussad.heimdall.server.core.entities.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "fr.jihadoussad.heimdall.server.core.*")
@EnableJpaRepositories
public class JPARepositoryApplication {

    public static void main(String[] args) {
        SpringApplication.run(JPARepositoryApplication.class);
    }

    static final String APP_NAME = "Avengers";
    static final Long EXPIRATION_TOKEN = 3000L;
    static final String LOGIN = "thor";
    static final String PASSWORD = "avengers";
    static final String DEFAULT_PRIVILEGE = "USER";
    static final String PRIVILEGE = "MEMBER";

    @Bean
    public CommandLineRunner loadData(final ApplicationRepository applicationRepository, final PrivilegeRepository privilegeRepository,
                                            final UserRepository userRepository) {
        return (args) -> {
            // create new application
            final Application avengers = new Application();
            avengers.setName(APP_NAME);
            avengers.setDefaultPrivilege(DEFAULT_PRIVILEGE);
            avengers.setExpirationToken(EXPIRATION_TOKEN);
            applicationRepository.save(avengers);

            // create Default avengers privilege
            final Privilege user = new Privilege();
            user.setName(DEFAULT_PRIVILEGE);
            user.setApplication(avengers);
            privilegeRepository.save(user);

            // create privilege for Avengers application
            final Privilege member = new Privilege();
            member.setName(PRIVILEGE);
            member.setApplication(avengers);
            privilegeRepository.save(member);

            // create new user for Avengers application
            final User thor = new User();
            thor.setLogin(LOGIN);
            thor.setPassword(PASSWORD);
            thor.addPrivileges(user, member);
            thor.setApplication(avengers);
            userRepository.save(thor);

            // create new application
            final Application foo = new Application();
            foo.setName("Foo");
            foo.setDefaultPrivilege(DEFAULT_PRIVILEGE);
            foo.setExpirationToken(EXPIRATION_TOKEN);
            applicationRepository.save(foo);

            // create Default avengers privilege
            final Privilege privilegeFoo = new Privilege();
            privilegeFoo.setName(DEFAULT_PRIVILEGE);
            privilegeFoo.setApplication(foo);
            privilegeRepository.save(privilegeFoo);

            // Flushing
            applicationRepository.flush();
            privilegeRepository.flush();
            userRepository.flush();
        };
    }

}
