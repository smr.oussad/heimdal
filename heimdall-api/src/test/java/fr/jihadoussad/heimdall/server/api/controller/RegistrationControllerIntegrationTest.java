package fr.jihadoussad.heimdall.server.api.controller;

import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;;
import org.springframework.http.MediaType;

import java.util.concurrent.atomic.AtomicReference;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class RegistrationControllerIntegrationTest extends CommonIntegrationTest {

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerAppWithInvalidLoginTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "?appName=app&password=password&defaultPrivilege=USER&expirationToken=3000")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_LOGIN.code);
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerAppWithInvalidPasswordTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "?appName=anotherApp&password=password&defaultPrivilege=USER&expirationToken=3000")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(INVALID_PASSWORD.code);
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerAppWhenExistTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(post(URL_REGISTER_SERVER + "?appName=appTest&password=P@ssw0rd&defaultPrivilege=USER&expirationToken=3000")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(APP_EXIST.code);
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerAppTest() throws Exception {
        mvc.perform(post(URL_REGISTER_SERVER + "?appName=myApp&password=P@ssw0rd&defaultPrivilege=USER&expirationToken=3000")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
