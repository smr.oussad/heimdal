package fr.jihadoussad.heimdall.server.api.controller;

import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.concurrent.atomic.AtomicReference;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.REQUIRED_FIELD;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RequestValidationIntegrationTest extends CommonIntegrationTest {

    @Test
    @Disabled //TODO remove when dockerfile done
    public void httpRequestMethodNotSupportedTest() throws Exception {
        mvc.perform(post(URL_OAUTH2_SERVER + "/heimdall/accessToken")
                .with(csrf())
                .header("Access-Control-Request-Method", "POST")
                .header("Origin", "http://heimdall-front-api.com")
                .header("Authorization", "Bearer " + appToken)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void httpMediaTypeNotSupportedTest() throws Exception {
        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void missingRequestFieldsTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(REQUIRED_FIELD.code);
    }
}
