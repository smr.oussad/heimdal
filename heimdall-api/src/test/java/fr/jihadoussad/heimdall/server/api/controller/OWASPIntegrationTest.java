package fr.jihadoussad.heimdall.server.api.controller;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OWASPIntegrationTest extends CommonIntegrationTest {

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerUserWhithOriginNotAllowedTest() throws Exception {
        getToken();

        mvc.perform(options(URL_REGISTER_SERVER + "/app/appTest/user?login=userTest&password=superCipherPassword&privileges=USER")
                .header("Access-Control-Request-Method", "POST")
                .header("Origin", "http://csrf-hacker.com")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void authenticateWithOriginNotAllowedTest() throws Exception {
        getToken();

        mvc.perform(options(URL_OAUTH2_SERVER + "/appTest/accessToken?login=foo&password=superCipherP@ssw0rd")
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://csrf-hacker.com")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void checkTokenWithOriginNotAllowedTest() throws Exception {
        getToken();

        mvc.perform(options("/oauth2/appTest/user/userTest/checkToken?token=my.hack.token")
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://csrf-hacker.com")
                .header("Authorization", "Bearer " + appToken)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void registerUserWithoutTokenAppTest() throws Exception {
        mvc.perform(post(URL_REGISTER_SERVER + "/app/appTest/user?login=userTest&password=superCipherPassword&privileges=USER")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void authenticateWhenAppTokenNotExistTest() throws Exception {
        mvc.perform(get(URL_OAUTH2_SERVER + "/fooApp/accessToken?login=foo&password=superCipherP@ssw0rd")
                .with(csrf())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
