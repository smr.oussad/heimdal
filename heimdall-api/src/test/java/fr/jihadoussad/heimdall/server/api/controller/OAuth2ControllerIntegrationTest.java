package fr.jihadoussad.heimdall.server.api.controller;

import fr.jihadoussad.heimdall.server.api.response.HeimdallResponse;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.concurrent.atomic.AtomicReference;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class OAuth2ControllerIntegrationTest extends CommonIntegrationTest {

    @Test
    @Disabled //TODO remove when dockerfile done
    public void authenticateWhenUserNotFoundTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken?login=foo&password=superCipherP@ssw0rd")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(USER_NOT_FOUND.code);
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void authenticateWhenPasswordIncorrectTest() throws Exception {
        final AtomicReference<String> atomicResponse = new AtomicReference<>("");
        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken?login=appTest&password=superCipherP@ssw0rd")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(mvcResult -> atomicResponse.set(mvcResult.getResponse().getContentAsString()))
                .andExpect(status().isBadRequest());

        final HeimdallResponse response = OBJECT_MAPPER.readValue(atomicResponse.get(), HeimdallResponse.class);

        assertThat(response.getCode()).isEqualTo(PASSWORD_INCORRECT.code);
    }

    @Test
    @Disabled //TODO remove when dockerfile done
    public void authenticateTest() throws Exception {
        mvc.perform(get(URL_OAUTH2_SERVER + "/accessToken?login=appTest&password=AppT3st!")
                .with(csrf())
                .header("Access-Control-Request-Method", "GET")
                .header("Origin", "http://heimdall-front-api.com")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
