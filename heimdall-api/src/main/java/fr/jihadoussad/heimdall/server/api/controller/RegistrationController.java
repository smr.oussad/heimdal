package fr.jihadoussad.heimdall.server.api.controller;

import fr.jihadoussad.heimdall.server.api.response.RegistrationAppResponse;
import fr.jihadoussad.heimdall.server.contract.RegistrationAppOutput;
import fr.jihadoussad.heimdall.server.contract.AuthenticationApplicationService;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import fr.jihadoussad.heimdall.server.contract.AuthorizationService;
import fr.jihadoussad.heimdall.utils.crypto.SecretKeyGenerator;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.security.*;
import java.util.*;

@RestController
@RequestMapping("register")
public class RegistrationController {

    private final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private final AuthorizationService authorizationService;

    private final AuthenticationApplicationService authenticationApplicationService;

    public RegistrationController(final AuthenticationApplicationService authenticationApplicationService,
                                  final AuthorizationService authorizationService) {
        this.authenticationApplicationService = authenticationApplicationService;
        this.authorizationService = authorizationService;
    }

    @ApiOperation(value="Register application", response = String.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message="Successful app register"),
            @ApiResponse(code=400, message="Contract input violation"),
            @ApiResponse(code=403, message ="Access denied"),
            @ApiResponse(code=500, message="Technical error")
    })
    @PostMapping(value = "/app", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RegistrationAppResponse> registerApp(@Param("appName") final String appName, @Param("password") final String password,
                                                               @Param("defaultPrivilege") final String defaultPrivilege, @Param(value = "expirationToken") final Long expirationToken)
            throws ContractValidationException, NoSuchAlgorithmException, IOException {
        logger.info("BEGIN - Process heimdall registration app with following input: appName = {}, password = *******, defaultPrivilege = {}, expirationToken = {}",
                appName, defaultPrivilege, expirationToken);
        logger.debug("Registration app input: appName = {}, password = {}, defaultPrivilege = {}, expirationToken = {}",
                appName, password, defaultPrivilege, expirationToken);

        logger.info("Process to token secret key generation...");
        final Key secretKey = SecretKeyGenerator.createHashmacSHA256Key();
        logger.debug("Secret key token generated: {}", Base64.getEncoder().encodeToString(secretKey.getEncoded()));

        logger.info("Call application management service to register {} app", appName);
        final RegistrationAppOutput registrationAppOutput = authenticationApplicationService.registerApplication(appName, password, defaultPrivilege, Base64.getEncoder().encodeToString(secretKey.getEncoded()), expirationToken);

        logger.info("Calling authentication application service to get expiration authentication token user");
        final Long expiration = authenticationApplicationService.getExpirationToken("heimdall");

        logger.info("Generating token jwt.");
        final String tokenKey = authorizationService.generateToken(secretKey, expiration, appName, "heimdall", registrationAppOutput.getPrivileges().toArray(String[]::new));

        final ResponseEntity<RegistrationAppResponse> response = ResponseEntity.ok(new RegistrationAppResponse(registrationAppOutput.getPublicKey(), tokenKey));

        logger.debug("Registration app response: publicKey = {}, token = {}", Objects.requireNonNull(response.getBody()).getPublicKey(), Objects.requireNonNull(response.getBody()).getTokenKey());
        logger.info("END - Successfully heimdall registration process. publicKey= {}, token = *******", Objects.requireNonNull(response.getBody()).getPublicKey());
        return response;
    }
}
