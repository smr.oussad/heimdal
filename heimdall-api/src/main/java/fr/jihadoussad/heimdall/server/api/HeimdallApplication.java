package fr.jihadoussad.heimdall.server.api;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import fr.jihadoussad.heimdall.client.HeimdallClient;
import fr.jihadoussad.heimdall.security.client.ClientWebSecurity;
import fr.jihadoussad.heimdall.security.server.common.MethodSecurityConfig;
import fr.jihadoussad.heimdall.security.server.web.ServerWebSecurity;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@SpringBootApplication
@ComponentScan(basePackages = {"fr.jihadoussad.heimdall.server.*"}, basePackageClasses = {ClientWebSecurity.class, HeimdallClient.class, MethodSecurityConfig.class})
@EntityScan(basePackages = {"fr.jihadoussad.heimdall.server.core.*"})
@EnableJpaRepositories(basePackages = {"fr.jihadoussad.heimdall.server.core.*"})
@EnableEncryptableProperties
public class HeimdallApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(final SpringApplicationBuilder builder) {
        return builder.sources(HeimdallApplication.class);
    }

    public static void main(final String[] args) {
        SpringApplication.run(HeimdallApplication.class);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
