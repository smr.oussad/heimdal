package fr.jihadoussad.heimdall.server.api.controller;

import fr.jihadoussad.heimdall.client.HeimdallClient;
import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import static fr.jihadoussad.heimdall.utils.api.ResponseCode.INVALID_PASSWORD_CIPHER;

@RestController
@RequestMapping("oauth2/heimdall")
public class OAuth2Controller {

    private final Logger logger = LoggerFactory.getLogger(OAuth2Controller.class);

    private final HeimdallClient heimdallClient;

    public OAuth2Controller(final HeimdallClient heimdallClient) {
        this.heimdallClient = heimdallClient;
    }

    @ApiOperation(value="Authenticate and generate token jwt", response = String.class)
    @ApiResponses(value= {
            @ApiResponse(code=200, message="Successful authenticate"),
            @ApiResponse(code=400, message="Contract input violation"),
            @ApiResponse(code=403, message ="Access denied"),
            @ApiResponse(code=500, message="Technical error")
    })
    @GetMapping(value = "/accessToken", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> authenticate(@RequestParam("login") final String login, @RequestParam("password") final String password)
            throws ContractValidationException {
        try {
            logger.info("Calling interal api to authenticate {} to the heimdall app...", login);
            return heimdallClient.authenticate(login, password);
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidKeySpecException e) {
            throw new ContractValidationException(INVALID_PASSWORD_CIPHER.message, INVALID_PASSWORD_CIPHER.code);
        }
    }
}
