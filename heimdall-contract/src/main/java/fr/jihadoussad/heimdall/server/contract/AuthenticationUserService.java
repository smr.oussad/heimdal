package fr.jihadoussad.heimdall.server.contract;

import fr.jihadoussad.heimdall.server.contract.exceptions.ContractValidationException;

import java.io.IOException;
import java.util.List;

/**
 * AuthenticationService
 */
public interface AuthenticationUserService {

    /**
     * This method permit to register a user in a target application.
     * @param login user to register.
     * @param encipheringPassword user to register.
     * @param secretKey the secret key.
     * @param appName target.
     * @param privileges user to register.
     * @return the complete privileges list.
     * @throws ContractValidationException if login or application name should not be filled correctly.
     * It exception can also be throws if login already exist or privilege not found.
     */
    List<String> registerUser(final String login, final String encipheringPassword, final String secretKey, final String appName, final String... privileges) throws ContractValidationException, IOException;

    /**
     * This method permit to authenticate user.
     * @param login user.
     * @param appName the application name.
     * @param encipheringPassword user.
     * @return privileges list.
     * @throws ContractValidationException if login or password invalid.
     */
    List<String> authenticate(final String login, final String appName, final String encipheringPassword) throws ContractValidationException, IOException;

    /**
     * This method permit user to modify his password.
     * @param login user.
     * @param appName the application name.
     * @param password user.
     * @throws ContractValidationException if user not found by the filled login or same password filled out.
     */
    void setPassword(final String login, final String appName, final String password) throws ContractValidationException;

    /**
     * This method permit to a user to unsubscribe. If user not found, fallback...
     * @param login user.
     * @param appName the application name.
     */
    void unsubscribe(final String login, final String appName);

    /**
     * This method permit to add privileges for a targeted user.
     * @param login user target.
     * @param appName the application name.
     * @param privileges to add.
     * @throws ContractValidationException if login or privilege name should not be filled correctly or not found.
     */
    void addPrivileges(final String login, final String appName, final String... privileges) throws ContractValidationException;

    /**
     * This method permit to remove privileges for a targeted user. If privileges not found, fallback...
     * @param login user target.
     * @param appName the application name.
     * @param privileges to add.
     * @throws ContractValidationException if login should not be filled correctly or not found.
     */
    void removePrivileges(final String login, final String appName, final String... privileges) throws ContractValidationException;

    /**
     * This method permit to get user secret key
     * @param login user target.
     * @param appName the application name.
     * @return the secret key
     * @throws ContractValidationException if user not found by application name and login filled out.
     */
    String getKey(final String login, final String appName) throws ContractValidationException;
}
