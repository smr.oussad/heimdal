package fr.jihadoussad.heimdall.server.contract.exceptions;

import java.util.StringJoiner;

/**
 * ContractValidationException
 */
public class ContractValidationException extends Exception {

    private final String code;

    public ContractValidationException(final String message, final String code) {
        super(message);
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ContractValidationException.class.getSimpleName() + "[", "]")
                .add("code='" + code + "'")
                .add("message='" + getMessage() + "'")
                .toString();
    }
}
