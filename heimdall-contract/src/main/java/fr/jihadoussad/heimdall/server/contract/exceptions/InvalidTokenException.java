package fr.jihadoussad.heimdall.server.contract.exceptions;

import static fr.jihadoussad.heimdall.utils.api.ResponseCode.INVALID_TOKEN;

/**
 * InvalidTokenException
 */
public class InvalidTokenException extends Exception {

    private final String code;

    public InvalidTokenException(final Throwable e) {
        super(INVALID_TOKEN.message, e);
        code = INVALID_TOKEN.code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return super.getMessage();
    }
}
