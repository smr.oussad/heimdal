package fr.jihadoussad.heimdall.server.contract;

import fr.jihadoussad.heimdall.server.contract.exceptions.InvalidTokenException;
import io.jsonwebtoken.ExpiredJwtException;

import java.security.Key;
import java.util.Map;

/**
 * Authorization service
 */
public interface AuthorizationService {

    /**
     *
     * @param secretKey the secret key generated
     * @param expiration the validity time after expiration
     * @param login user
     * @param privileges user
     * @param appName application name
     * @return the Json Web Token
     */
    String generateToken(final Key secretKey, final long expiration, final String login, final String appName, final String... privileges);

    /**
     * Get the token payload
     * @param token jwt (header + payload + signature)
     * @return the token payload
     * @throws InvalidTokenException if the token is invalid
     */
    Map<String, Object> getPayload(final String token) throws InvalidTokenException;

    /**
     * Verify if the token is valid
     * @param token jwt (header + payload + signature)
     * @param secretKey the secret key
     * @return information about this token (subject, expiration etc...)
     * @throws InvalidTokenException if the token is invalid
     * @throws ExpiredJwtException if the token has expired
     */
    Map<String, Object> verifyToken(final String token, final Key secretKey) throws InvalidTokenException, ExpiredJwtException;
}
