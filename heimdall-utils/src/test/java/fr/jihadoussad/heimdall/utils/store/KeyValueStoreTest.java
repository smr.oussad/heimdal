package fr.jihadoussad.heimdall.utils.store;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

class KeyValueStoreTest {

    private static final String PATH = "target/heimdall-key";
    private static final String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFXv3QyNNm5YDxM3DFGK8kA7QhW8bdcX2UzVuELI+Fvbik4TgZf1yJ9+B8dcNSitydp8F8U5C7Mt58wH7A0H+9Db8uC747z76Ut+N4MQCqlSA+q2QhIvmIeP2PufK5lcUwhkTR7VmddKzZQKoYey6dx2L05RUjihHj+ANyDS0ttvCWiPI7EhDcqgETYHbbpSXu5IFahTCN0y9UDDm7DYYW8eeQPsYGu5HAeHW2SJoqNExeAeE0G9b+uZ8qOx23M9ILs2cAjO3LdMnYwSYhE2AE1MpTJrNwwDSLFO7hd/qEq4lrY9C3wMajbefHd6Sro90Q0WFhZse68Ny+iM70t4WnAgMBAAECggEADfEvMVs9Fj2ZrB1HhaMd8/LbKr2QXWfWy+5nybpqyn1ZxwqwNaIlx1p9QiWBZKiVhXma9Wc68wbPUVVCKt/g6p98Q9EAcvo1nxBWHCNrQWnTdBmwnvIsCcM0Jok2wYXRllAC87sPKjGfPBrRbTu7VM0aICWufweY9S7hFVQhS+zz20lI5TPcWoxNPnli1C6XR/QmdxSc6QYQ4fJu86xEXnuW52T+Fng5IPTCq4/8I5Si0GO4tHUZwYnFtDOAo6yzLDtHAar7LSd9hagRMqxRkoE+cmWamBTZACb4kyUz08hJbQdP61pnK169WUWQKBsP/c5A+saqHOTL4oKXctTysQKBgQDUH9oOitw48EqQf7bP5XoKQFHmusIW2pfxRaKE3ciVV8iQwo42rSiFmHGPGoURBh11hhpfr7wYkNVDkubbFWzsToeJR8iJ37UbFm+w/rNY28QW4u89QphVxoEx7uFS39OxxuEBu4keQ0wnellftgs/D8WYmdTIzFqnWoAu9yATXwKBgQCg9Rb8jnCZxF2nZvxe+M5FjL1WAo6Zseyp2+smHoDpnneSGu5Fm6CW2vGA69qQl9womdr8CUH0XPHzQ5UY/6rEEvLfcVKAie9WYY2ywwh35xUTHF0XIYJemYgW/00xV1cRjKSzweAzKhDCgWXPkLKVYJVznpea4VMuR5RqbZo6uQKBgAbaJIwVtjlsoav7RCeBDSkpnLusSrq8Qu47Rp2uIF2aoVLzxe5EN6vhGcH6pFPBc9ApDc8LeTL98rWlGCn5jjQv8TbhB7xUaqA+YEtCXv6+0p2+pOXAsfULXX8EbWjk7AyxFPGeYjaFC/+2dw6dbc2Sr9pXFcWKvF7tkHhz6ShXAoGAfMI8VPpbUPSh2xW6XCriR4p1X2uP+JWu6WIq/Fr8dRh9eeQh+BZO/V8rflBSE22CGn5W/LB4TN6WM4aY1M2nIV/GnWcLGdLQjtYCrSL7N2UIMs3zYfB5SrYahCjTPoGCXXn0V9jY3Q+n+fuuXJq9sYcxkHLG9Cfe/6o/4daeixECgYAge3kEGgMXClw3GevkpJ0iATbZ1CAT8Vys39zfuyNM889EDvyXRqDmarqNxVBLy5+8uu1hqhurqDhJ7Qy96mjJ8c0edl1EPuenZ6TZXHkXBFFfH/zAo3dnIjfGvKZqi0UbXD8JMkoLdGH2ePH0wX2/DwYF2stPopqd7D5F+9dPoQ==";
    private static final String publicKey = "MIIGvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCFXv3QyNNm5YDxM3DFGK8kA7QhW8bdcX2UzVuELI+Fvbik4TgZf1yJ9+B8dcNSitydp8F8U5C7Mt58wH7A0H+9Db8uC747z76Ut+N4MQCqlSA+q2QhIvmIeP2PufK5lcUwhkTR7VmddKzZQKoYey6dx2L05RUjihHj+ANyDS0ttvCWiPI7EhDcqgETYHbbpSXu5IFahTCN0y9UDDm7DYYW8eeQPsYGu5HAeHW2SJoqNExeAeE0G9b+uZ8qOx23M9ILs2cAjO3LdMnYwSYhE2AE1MpTJrNwwDSLFO7hd/qEq4lrY9C3wMajbefHd6Sro90Q0WFhZse68Ny+iM70t4WnAgMBAAECggEADfEvMVs9Fj2ZrB1HhaMd8/LbKr2QXWfWy+5nybpqyn1ZxwqwNaIlx1p9QiWBZKiVhXma9Wc68wbPUVVCKt/g6p98Q9EAcvo1nxBWHCNrQWnTdBmwnvIsCcM0Jok2wYXRllAC87sPKjGfPBrRbTu7VM0aICWufweY9S7hFVQhS+zz20lI5TPcWoxNPnli1C6XR/QmdxSc6QYQ4fJu86xEXnuW52T+Fng5IPTCq4/8I5Si0GO4tHUZwYnFtDOAo6yzLDtHAar7LSd9hagRMqxRkoE+cmWamBTZACb4kyUz08hJbQdP61pnK169WUWQKBsP/c5A+saqHOTL4oKXctTysQKBgQDUH9oOitw48EqQf7bP5XoKQFHmusIW2pfxRaKE3ciVV8iQwo42rSiFmHGPGoURBh11hhpfr7wYkNVDkubbFWzsToeJR8iJ37UbFm+w/rNY28QW4u89QphVxoEx7uFS39OxxuEBu4keQ0wnellftgs/D8WYmdTIzFqnWoAu9yATXwKBgQCg9Rb8jnCZxF2nZvxe+M5FjL1WAo6Zseyp2+smHoDpnneSGu5Fm6CW2vGA69qQl9womdr8CUH0XPHzQ5UY/6rEEvLfcVKAie9WYY2ywwh35xUTHF0XIYJemYgW/00xV1cRjKSzweAzKhDCgWXPkLKVYJVznpea4VMuR5RqbZo6uQKBgAbaJIwVtjlsoav7RCeBDSkpnLusSrq8Qu47Rp2uIF2aoVLzxe5EN6vhGcH6pFPBc9ApDc8LeTL98rWlGCn5jjQv8TbhB7xUaqA+YEtCXv6+0p2+pOXAsfULXX8EbWjk7AyxFPGeYjaFC/+2dw6dbc2Sr9pXFcWKvF7tkHhz6ShXAoGAfMI8VPpbUPSh2xW6XCriR4p1X2uP+JWu6WIq/Fr8dRh9eeQh+BZO/V8rflBSE22CGn5W/LB4TN6WM4aY1M2nIV/GnWcLGdLQjtYCrSL7N2UIMs3zYfB5SrYahCjTPoGCXXn0V9jY3Q+n+fuuXJq9sYcxkHLG9Cfe/6o/4daeixECgYAge3kEGgMXClw3GevkpJ0iATbZ1CAT8Vys39zfuyNM889EDvyXRqDmarqNxVBLy5+8uu1hqhurqDhJ7Qy96mjJ8c0edl1EPuenZ6TZXHkXBFFfH/zAo3dnIjfGvKZqi0UbXD8JMkoLdGH2ePH0wX2/DwYF2stPopqd7D5F+9dPoQ==";

    @Test
    public void keyValueStoreLoadValueWithFileNotFoundTest() throws IOException {
        final String key = "private.key";
        KeyValueStore.save(PATH, key, privateKey);

        assertThat(KeyValueStore.load(PATH, key))
                .isEqualTo(privateKey);
    }

    @Test
    public void keyValueStoreLoadValueTest() throws IOException {
        final String anotherKey = "public.key";
        KeyValueStore.save(PATH, anotherKey, publicKey);

        assertThat(KeyValueStore.load(PATH, anotherKey))
                .isEqualTo(publicKey);
    }

    @Test
    public void keyValueStoreErasePropertyTest() throws IOException {
        final String key = "private.key";
        KeyValueStore.save(PATH, key, publicKey);

        KeyValueStore.save(PATH, key, privateKey);

        assertThat(KeyValueStore.load(PATH, key))
                .isEqualTo(privateKey);
    }
}