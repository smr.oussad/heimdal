package fr.jihadoussad.heimdall.utils.crypto;

import org.junit.jupiter.api.Test;

import java.security.*;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

class SecretKeyGeneratorTest {

    @Test
    public void StringSecretToKey() throws NoSuchAlgorithmException {
        // Create HMAC HMAC-SHA256 secret key
        final Key key = SecretKeyGenerator.createHashmacSHA256Key();
        final String keyString = Base64.getEncoder().encodeToString(key.getEncoded());

        assertThat(Base64.getEncoder().encodeToString(SecretKeyGenerator.toKey(keyString).getEncoded()))
                .isEqualTo(keyString);
    }
}