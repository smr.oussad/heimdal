package fr.jihadoussad.heimdall.utils.validators;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class InputValidatorTest {

    /*********************************************** LOGIN *********************************************/

    @Test
    public void short_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("log"))
                .isFalse();
    }

    @Test
    public void long_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("logiiiiiiiiiiiiiiiiin"))
                .isFalse();
    }

    @Test
    public void login_should_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("login"))
                .isTrue();
    }

    @Test
    public void login_with_invalid_character() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("<html>blop</html>"))
                .isFalse();
    }

    @Test
    public void login_with_underscore_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("my_login"))
                .isTrue();
    }

    @Test
    public void login_with_underscore_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("_mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_underscore_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("mylogin_"))
                .isFalse();
    }

    @Test
    public void login_with_dot_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("my.login"))
                .isTrue();
    }

    @Test
    public void login_with_dot_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate(".mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_dot_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("mylogin."))
                .isFalse();
    }

    @Test
    public void login_with_score_inside_should_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("my-login"))
                .isTrue();
    }

    @Test
    public void login_with_score_at_the_begining_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("-mylogin"))
                .isFalse();
    }

    @Test
    public void login_with_score_at_the_end_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("mylogin-"))
                .isFalse();
    }

    @Test
    public void owasp_sql_injection_login_should_not_be_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.LOGIN).validate("SELECT * FROM ADMIN"))
                .isFalse();
    }

    /*********************************************** PASSWORD *********************************************/

    @Test
    public void password_with_more_than_32_characters() {
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("Abcdefghijklm-nopqrstuvwxyz123456"))
                .isFalse();
    }

    @Test
    public void password_with_less_than_8_characters() {
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("Abc-def"))
                .isFalse();
    }

    @Test
    public void password_with_no_upper_case_and_no_special_character() {
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("abcdefghi"))
                .isFalse();
    }

    @Test
    public void password_with_more_than_2_equal_character() {
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("Abbbe-fghi"))
                .isFalse();
    }

    @Test
    public void password_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("Abcde-fghi"))
                .isTrue();
    }

    @Test
    public void owasp_sql_injection_password_should_not_be_valid(){
        assertThat(InputValidator.getInstance(RegexValidator.PASSWORD).validate("Delete * from admin"))
                .isFalse();
    }

    /*********************************************** URL *********************************************/

    @Test
    public void url_invalid() {
        assertThat(InputValidator.getInstance(RegexValidator.URL).validate("heimdall.com"))
            .isFalse();
    }

    @Test
    public void url_http_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.URL).validate("http://heimdall.com"))
                .isTrue();
    }

    @Test
    public void url_https_valid() {
        assertThat(InputValidator.getInstance(RegexValidator.URL).validate("https://heimdall.com"))
                .isTrue();
    }
}