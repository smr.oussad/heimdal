package fr.jihadoussad.heimdall.utils.crypto;

import org.junit.jupiter.api.Test;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

class KeyPairGeneratorTest {

    @Test
    public void cipherDecipherTest()
            throws NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, BadPaddingException, InvalidKeySpecException, InvalidKeyException {
        // Generate RSA KeyPair
        final KeyPair keyPair = KeyPairGenerator.createRSAKeyPair();

        final String data = "superCipherP@ssw0rd";

        final String dataEncoded = KeyPairGenerator.cipher(Base64.getEncoder().encodeToString(keyPair.getPublic().getEncoded()), data);


        assertThat(KeyPairGenerator.decipher(Base64.getEncoder().encodeToString(keyPair.getPrivate().getEncoded()), dataEncoded))
                .isEqualTo(data);
    }
}