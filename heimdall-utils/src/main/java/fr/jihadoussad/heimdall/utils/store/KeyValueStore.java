package fr.jihadoussad.heimdall.utils.store;

import java.io.*;
import java.util.Properties;

/**
 * KeyValueStore
 */
public class KeyValueStore {

    private KeyValueStore() {}

    public static void save(final String path, final String key, final String value) throws IOException {
        Properties properties;

        try {
            properties = load(path);
        } catch (final FileNotFoundException e) {
            properties = new Properties();
        }

        try (final OutputStream output = new FileOutputStream(path)) {
            properties.put(key, value);
            properties.store(output, null);
        }
    }

    public static String load(final String path, final String key) throws IOException {
        return load(path).getProperty(key);
    }

    private static Properties load(final String path) throws IOException {
        try(final InputStream input = new FileInputStream(path)) {
            final Properties properties = new Properties();

            // load a properties file
            properties.load(input);

            return properties;
        }
    }
}
