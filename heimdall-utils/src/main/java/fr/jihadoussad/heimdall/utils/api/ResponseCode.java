package fr.jihadoussad.heimdall.utils.api;

public enum ResponseCode {

    APP_NOT_FOUND("01", "Application not exist ! Please verify the application name and try again."),
    APP_EXIST("02", "Application name already exist !"),
    PRIV_EXIST_APP("03", "(Some of) Privilege(s) already exist for this application: "),
    PRIV_NOT_FOUND("04", "Privilege(s) name filled not found ! Please verify it and try again."),
    PRIV_EXIST_USER("05", "(Some of) Privilege(s) already exist for this user : "),
    DEF_PRIV_NOT_DELETED("06", "Default privilege not be able to remove !"),
    USER_NOT_FOUND("07", "User not found ! Please check your login and try again."),
    USER_EXIST("08", "Login already exist !"),
    INVALID_LOGIN("09", "Invalid login ! 4 to 20 character login requiring (only alphanumeric and dot, underscore, score authorized)."),
    INVALID_PASSWORD("11", "Invalid password ! 8 to 32 character password requiring at least.\n" +
            "Moreover, your password should contains one uppercase and special character at least.\n" +
            "Finally, it should contains no more than 2 equal characters"),
    REQUIRED_FIELD("12", "Some fields should not filled: "),
    PASSWORD_INCORRECT("13", "Password incorrect !"),
    SAME_PASSWORD("14", "Actual password is the same ! Please use another password"),
    INVALID_PASSWORD_CIPHER("15", "Deciphering password failure ! Please check if you have correctly encrypt it and try again."),
    INVALID_TOKEN("16", "Invalid token !"),
    TOKEN_EXPIRED("17", "Token expired ! You need to authenticate again to get new valid token."),
    TECHNICAL_ERROR("99", "Technical error was occurred !");

    public final String code;

    public final String message;

    ResponseCode(final String code, final String message) {
        this.code =  code;
        this.message = message;
    }
}
