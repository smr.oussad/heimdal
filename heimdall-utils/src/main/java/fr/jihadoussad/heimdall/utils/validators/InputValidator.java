package fr.jihadoussad.heimdall.utils.validators;

import java.util.regex.Pattern;

/**
 * Input validator
 */
public class InputValidator {

    private static Pattern pattern;

    private InputValidator(final RegexValidator regexValidator) {
        pattern = regexValidator.pattern;
    }

    public static InputValidator getInstance(final RegexValidator regexValidator) {
        return new InputValidator(regexValidator);
    }

    public boolean validate(final String input) {
        return pattern.matcher(input).matches();
    }
}
