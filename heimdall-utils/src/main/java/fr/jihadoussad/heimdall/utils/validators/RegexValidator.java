package fr.jihadoussad.heimdall.utils.validators;

import java.util.regex.Pattern;

/**
 * Regex validator
 */
public enum RegexValidator {

    /**
     * Description: 4 to 20 character login requiring (only alphanumeric and dot, underscore, score authorized)
     */
    LOGIN(Pattern.compile("^(?=.{4,20}$)(?![_.-])(?!.*[_.-]{2})[a-zA-Z0-9._-]+(?<![_.-])$")),

    /**
     * Source: https://owasp.org/www-community/OWASP_Validation_Regex_Repository
     * Description: 8 to 32 character password requiring at least
     * Moreover one Uppercase and special character is required
     * and no more than 2 same characters in a row
     */
    PASSWORD(Pattern.compile("^(?:(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))(?!.*(.)\\1{2,})[A-Za-z0-9!~<>,;:_=?*+#.\"&§%°()|\\[\\]\\-$^@/]{8,32}$")),

    /**
     * Source: https://owasp.org/www-community/OWASP_Validation_Regex_Repository
     */
    URL(Pattern.compile("^(((https?)://)(%[0-9A-Fa-f]{2}|[-()_.!~*';/?:@&=+$,A-Za-z0-9])+)([).!';/?:,])?$"));

    public final Pattern pattern;

    RegexValidator(final Pattern pattern) {
        this.pattern = pattern;
    }
}
