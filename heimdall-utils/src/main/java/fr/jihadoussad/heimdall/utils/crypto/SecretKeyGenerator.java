package fr.jihadoussad.heimdall.utils.crypto;

import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.util.Base64;

/**
 * SecretKeyGenerator
 */
public final class SecretKeyGenerator {
    
    private SecretKeyGenerator() {}

    /**
     * Generate a Key with HMAC-SHA256 algorithm
     * @return a Key
     * @throws NoSuchAlgorithmException when not found actual algorithm
     */
    public static Key createHashmacSHA256Key() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance("HmacSHA256").generateKey();
    }

    public static Key toKey(final String secretKey) {
        final byte[] decodeKey = Base64.getDecoder().decode(secretKey);

        return new SecretKeySpec(decodeKey, 0, decodeKey.length, "HmacSHA256");
    }
}
