package fr.jihadoussad.heimdall.server.api.response;

public class RegistrationAppResponse {

    private String publicKey;

    private String tokenKey;

    public RegistrationAppResponse() {
    }

    public RegistrationAppResponse(String publicKey, String tokenKey) {
        this.publicKey = publicKey;
        this.tokenKey = tokenKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public String getTokenKey() {
        return tokenKey;
    }
}
