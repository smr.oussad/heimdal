import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AccountManagementModule } from "./account-management/account-management.module";
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {FormsModule} from "@angular/forms";

import {AuthService} from "./authentication/service/auth.service";

@NgModule({
  declarations: [
    AppComponent,
    AuthenticationComponent,
    HomeComponent,
    RegistrationComponent,
    PageNotFoundComponent
  ],
    imports: [
        BrowserModule,
        AccountManagementModule,
        AppRoutingModule,
        FormsModule,
        HttpClientModule
    ],
  providers: [
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
