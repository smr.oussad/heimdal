package fr.jihadoussad.heimdall.client;

import fr.jihadoussad.heimdall.utils.crypto.KeyPairGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class HeimdallClient {

    private final Logger logger = LoggerFactory.getLogger(HeimdallClient.class);

    private final RestTemplate restTemplate;

    @Value("${heimdall.pub.key}")
    private String publicKey;

    @Value("${heimdall.oauth2.url}")
    private String oauth2Url;

    @Value("${heimdall.registration.url}")
    private String registrationUrl;

    @Value(("${heimdall.app.token}"))
    private String token;

    public HeimdallClient() {
        this.restTemplate = new RestTemplate();

        final DefaultUriBuilderFactory builderFactory = new DefaultUriBuilderFactory();
        builderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.VALUES_ONLY);

        restTemplate.setUriTemplateHandler(builderFactory);
        restTemplate.setInterceptors(Collections.singletonList(new PlusEncoderInterceptor()));
    }

    public ResponseEntity<String> authenticate(final String login, final String password)
            throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        logger.info("Prepare request and url param to call heimdall authentication user api");
        logger.debug("Client authentication input: login = {}, password = {}", login, password);
        final Map<String, String> urlParams = new HashMap<>();

        final Map<String, String> requestParams = new HashMap<>();
        requestParams.put("login", login);
        requestParams.put("password", KeyPairGenerator.cipher(publicKey, password));

        return getResponseEntity(oauth2Url + "/accessToken", HttpMethod.GET, urlParams, requestParams, new ParameterizedTypeReference<String>() {});
    }

    public ResponseEntity<Map<String, Object>> checkToken(final String token) {
        logger.info("Prepare request and url param to call heimdall token checker api");
        logger.debug("Client token checker input: token = {}", token);
        final Map<String, String> urlParams = new HashMap<>();

        final Map<String, String> requestParams = new HashMap<>();
        requestParams.put("token", token);

        return getResponseEntity(oauth2Url + "/checkToken", HttpMethod.GET, urlParams, requestParams, new ParameterizedTypeReference<Map<String, Object>>() {});
    }

    public ResponseEntity<String> register(final String login, final String password)
            throws NoSuchPaddingException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidKeyException, InvalidKeySpecException {
        logger.info("Prepare request and url param to call heimdall registration user api");
        logger.debug("Client registration input: login = {}, password = {}", login, password);
        final String encryptedPassword = KeyPairGenerator.cipher(publicKey, password);

        final Map<String, String> urlParams = new HashMap<>();

        final Map<String, String> requestParams = new HashMap<>();
        requestParams.put("login", login);
        requestParams.put("password", encryptedPassword);

        return getResponseEntity(registrationUrl, HttpMethod.POST, urlParams, requestParams, new ParameterizedTypeReference<String>() {});
    }

    <T> ResponseEntity<T> getResponseEntity(final String uri, final HttpMethod httpMethod,
                                                   final Map<String, String> urlParams, final Map<String, String> requestParams,
                                                   final ParameterizedTypeReference<T> type) {
        // Query parameters
        final UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uri);
        requestParams.forEach(builder::queryParam);

        // Set the Authorization token app in the headers
        final HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HttpEntity<Map<String, Object>> entity = new HttpEntity<>(headers);

        return restTemplate.exchange(builder.buildAndExpand(urlParams).encode().toUri(), httpMethod,
                entity, type);
    }
}
